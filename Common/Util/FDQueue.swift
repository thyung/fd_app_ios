/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class FDQueue<T>: NSObject {
    private var array: [T] = []
    private let accessQueue = DispatchQueue(label: "FD_EVENT_QUEUE", attributes: .concurrent)
    
    var count: Int {
        var count = 0
        accessQueue.sync {
            count = self.array.count
        }
        return count
    }

    func enqueue(_ element: T) {
        accessQueue.async {
            self.array.append(element)
        }
    }
    
    func dequeue() -> T? {
        var element: T? = nil
        accessQueue.sync {
            element = self.array.first
            if self.array.count > 0 {
                self.array.remove(at: 0)
            }
        }
        return element
    }
    
    func clear() {
        accessQueue.async {
            self.array.removeAll()
        }
    }    
    
    func remove(at: Int){
        accessQueue.async {
            self.array.remove(at: at)
        }
    }
    
    func first() -> T? {
        var element: T?
        accessQueue.sync {
            element = self.array.first
        }
        return element
    }
    
    public subscript(index: Int) -> T {
        set(newValue) {
            self.accessQueue.async() {
                self.array[index] = newValue
            }
        }
        get {
            var element: T!
            self.accessQueue.sync() {
                element = self.array[index]
            }
            return element
        }
    }
}
