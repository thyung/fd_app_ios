/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

extension Data {

    func dictionaryFromJson() -> [String : Any]? {
        do {
            let dic = try JSONSerialization.jsonObject(with: self,
                                                       options: [.mutableContainers]) as? [String : Any]
            return dic
        } catch let error {
            LOGD(to: "\(#function) error: \(error)")
            return nil
        }
    }
    
    func arrayFromJson() -> [[String : Any]]? {
        do {
            let array = try JSONSerialization.jsonObject(with: self,
                                                         options: [.mutableContainers]) as? [[String : Any]]
            return array
        } catch let error {
            LOGD(to: "\(#function) error: \(error)")
            return nil
        }
    }
}
