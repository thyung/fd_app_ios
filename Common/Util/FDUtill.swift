/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class FDUtill: NSObject {
    class func loadFromDefaultWithList(key: String) -> Any? {
        return (UserDefaults.standard.value(forKey: key))
    }
    
    class func updateUserDefault(_ key: String, value: Any) -> Bool {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        return defaults.synchronize()
    }
    
    // MARK: - File manage
    class func writeImagefile(name: String, image: UIImage?, overWrite: Bool) -> String {
        var ret: String = "ERRORO"
        let count = 0
        guard count != name.count else { return "Invalid params." }
        if image == nil { return "Invalid params." }
                
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let dir = (paths.first! as NSString).appendingPathComponent("fd_image")
            if FileManager.default.fileExists(atPath: dir) == false {
                do {
                    try FileManager.default.createDirectory(
                        atPath: dir, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    LOGD(to: "Error creating directory: \(error.localizedDescription)")
                    return error.localizedDescription
                }
            }
            let filePath = (dir as NSString).appendingPathComponent("\(name).4ds")
            let url = URL.init(fileURLWithPath: filePath)
            do {
                if FileManager.default.fileExists(atPath: filePath) == false ||  overWrite == true {
                    try image?.jpegData(compressionQuality: 1.0)?.write(to: url, options: .atomic)
                } else {
                    LOGD(to: "File exist and over write: \(overWrite ? "true" : "false") \n \(filePath)")
                }
            } catch let error as NSError {
                LOGD(to: "Error creating directory: \(error.localizedDescription)")
                return error.localizedDescription
            }
            ret = name
        }
        return ret
    }
    
    class func loadImageFile(name: String) -> UIImage? {
        var ret: UIImage? = nil
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let base = paths.first            
            let pathUrl = URL(fileURLWithPath: base!).appendingPathComponent("fd_image/\(name).4ds")
            ret = UIImage(contentsOfFile: pathUrl.path)
        }
        return ret
    }
    
    @discardableResult
    class func removeImageFile(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if paths.count > 0 {
            let dir = (paths.first! as NSString).appendingPathComponent("fd_image")
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: dir)
                for file in files {
                    if file.hasSuffix(".4ds") {
                        let filePath = (dir as NSString).appendingPathComponent("\(name).4ds")
                        if FileManager.default.fileExists(atPath: filePath) == true {
                            try FileManager.default.removeItem(atPath: filePath)
                        }
                    }
                }
            } catch let error as NSError {
                LOGD(to: "Error creating directory: \(error.localizedDescription)")
                return error.localizedDescription
            }
        }
        return name
    }
        
    // .plist
    class func loadPlistWithDictionary(_ fileName: String) ->Array<[String : AnyObject]>? {
        var retValue: Array<[String : AnyObject]>? = nil
        guard let path: String = Bundle.main.path(forResource: fileName, ofType: "plist") else {
            LOGD(to: "Fail to load file: \(fileName)")
            return nil
        }
        var format = PropertyListSerialization.PropertyListFormat.xml
        guard let aData: Data = FileManager.default.contents(atPath: path) else {
            LOGD(to: "Fail to load contents: \(path)")
            return nil
        }
        do {
            try retValue = PropertyListSerialization.propertyList(from: aData,
                                                                  options: .mutableContainersAndLeaves,
                                                                  format: &format) as? Array<[String : AnyObject]>
        } catch let error as NSError {
            LOGD(to: "Error property list serialization: \(error)")
        }
        return retValue
    }
    
    class func getDuration(_ value: Int32) -> String{
        let sign = Int(value).signum()
        let seconds = abs(value)/1000
        var ms = NSMutableString.init(capacity: 8)
        let h = seconds / 3600
        let m = seconds / 60 % 60
        let s = seconds % 60
        
        if seconds < 0 {
            ms = "∞"
        } else {
            if sign < 0 { ms = "- " }
            if h > 0 { ms.appendFormat("%d:", h) }
            if m < 10 { ms.append("0") }
            ms.appendFormat("%d:", m)
            if s < 10 { ms.append("0") }
            ms.appendFormat("%d", s)
        }
        return ms as String
    }
    
    class func getDateFromString(_ format: String, date: String, timeZone: String?) -> String {
        let formatter = DateFormatter()
        if timeZone != nil {
            formatter.timeZone = TimeZone(identifier: timeZone!)
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            let aDate: Date = formatter.date(from: date)!
            formatter.dateFormat = format
            return formatter.string(from: aDate)
        } else {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            let aDate: Date = formatter.date(from: date)!
            formatter.dateFormat = format
            return formatter.string(from: aDate)
        }
    }
    
    class func checkNetwork() -> Bool {
        var isConnect = false
        let reach = Reachability.forInternetConnection()
        let status = reach?.currentReachabilityStatus()
       
        switch status {
        case NotReachable:
            LOGD(to: "Device is not connected.")
            isConnect = false
            break
        case ReachableViaWiFi:
            LOGD(to: "Device is connected to WiFi.")
            isConnect = true
            break
        case ReachableViaWWAN:
            LOGD(to: "Device is connected to WWAN.")
            isConnect = true
            break
        default:
            break
        }
        
        return isConnect
    }
}

@propertyWrapper
struct Atomic<T> {
    
    private var t: T
    private let lock = NSLock()
    
    init(wrappedValue t: T) {
        self.t = t
    }
    
    var wrappedValue: T {
        get { return load()}
        set { store(new: newValue) }
    }
    
    func load() -> T {
        lock.lock()
        defer { lock.unlock() }
        return t
    }
    
    mutating func store(new: T) {
        lock.lock()
        defer { lock.unlock() }
        t = new
    }
}
