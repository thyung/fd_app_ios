/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import Foundation

extension String {
     func dictionaryFromJson() -> [String : String]? {
        var ret: [String : String]? = nil
        guard let data = self.data(using: .utf8) else {
            LOGD(to: "Fail to encoded(\(self))")
            return ret
        }
                        
        do {
            ret = try JSONSerialization.jsonObject(with: data, options: .mutableContainers ) as? [String : String]
        } catch let error  {
            LOGD(to: "\(#function) error: \(error)")
        }
        
        return ret
    }
    
    func arrayFromJson() -> [ [String : String]?]? {
        var ret: [ [String : String]?]? = nil
        guard let data = self.data(using: .utf8) else {
            LOGD(to: "Fail to encoded(\(self))")
            return ret
        }
        
        do {
            ret = try JSONSerialization.jsonObject(with: data, options: .mutableContainers ) as? [[String : String]?]
        } catch let error  {
            LOGD(to: "\(#function) error: \(error)")
        }
        
        return ret
    }
}

