/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class BaseView: UIView {

    @IBOutlet weak var indicator: UIActivityIndicatorView?
    @IBOutlet weak var vBlock: UIView?
        
    internal var toastTimer: Timer?
    
    open func initVariable() {
    }
    
    open func setupLayout() {
    }
    
    open func showIndicator() {
        DispatchQueue.main.async {
            if let view = self.vBlock {
                view.isHidden = false
                view.bringSubviewToFront(view)
            }
            if let view = self.indicator {
                view.isHidden = false
                view.startAnimating()
            }
        }
    }
    
    open func closeIndicator() {        
        DispatchQueue.main.async {
            if let view = self.vBlock {
                view.isHidden = true
            }
            if let view = self.indicator {
                view.isHidden = true
                view.stopAnimating()
            }
        }
    }
    
    open func beginTimer() {
        invalidateTimer()
        sleep(UInt32(0.2))
        toastTimer = Timer.scheduledTimer(timeInterval: 3, target: self,
                                     selector: #selector(onTick(timer:)), userInfo: nil, repeats: false)
    }
    
    internal func invalidateTimer() {
        if toastTimer != nil {
            toastTimer?.invalidate()
            toastTimer = nil
        }
    }
    
    @objc private func onTick(timer: Timer) {
        self.closeToastMessage()
    }
}

extension BaseView {
    // MARK: - Util
    func showToastMessage(_ message: String) {
        if message.count < 1 { return }
        
        var rect = self.frame
        var vBackground: UIView!
        var lbText: UILabel!
        let ivBackground: UIImageView!
        
        let font = UIFont.systemFont(ofSize: 14)
        let bString: NSString = message as NSString
        let size =  bString.size(withAttributes: [NSAttributedString.Key.font: font])
        LOGD(to: "Message size: \(size)");
        
        if let view = self.viewWithTag(9001) {
            vBackground = view
        } else {
            vBackground = UIView.init(frame: rect)
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(taptHandler(_:)))
            vBackground.addGestureRecognizer(tapRecognizer)
        }
        vBackground.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        vBackground.tag = 9001
        
        var ret = size.width / (rect.size.width - 64)
        if ret > 0 {
            ret = ceil(ret)
            rect.size.height = ret * size.height + 32
            rect.size.width = rect.width - 32
        }
        
        let bgImage = UIImage(named: "notice_bg")
        var frame = CGRect.init(x: 16, y: 16, width: rect.width, height: rect.height)
        
        if let imageView = self.viewWithTag(9002) {
            ivBackground = imageView as? UIImageView
            ivBackground.frame = frame
        } else {
            ivBackground = UIImageView.init(frame: frame)
            ivBackground.image = bgImage
            ivBackground.tag = 9002
            vBackground.addSubview(ivBackground)
        }
        
        if let labe = self.viewWithTag(9003) {
            lbText = labe as? UILabel
        } else {
            lbText = UILabel()
            lbText.numberOfLines = 3
            lbText.tag = 9003
        }
        frame.origin.x = frame.origin.x + 16
        frame.size.width = rect.width - 32
        lbText.frame = frame
        lbText.text = message
        
        vBackground.addSubview(lbText)
        self.addSubview(vBackground);
        vBackground.fadeIn()
        
        self.beginTimer()
    }
    
    @objc func taptHandler(_ recognizer: UITapGestureRecognizer) {
        closeToastMessage()
    }
    
    func closeToastMessage() {
        if let view = self.viewWithTag(9001) {
            view.removeFadeOut()
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func showMessageAlert(message: String, title: String) {
        let controller = UIAlertController.init(title: title ,
                                                message: message ,
                                                preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) {(action) in
        }
        controller.addAction(okAction)
        if let parent = self.parentViewController {
            parent.present(controller, animated: true, completion: nil)
        }
    }
}

extension UIView {
    // MARK: - Animation
    func fadeIn(duration: TimeInterval = 0.4) {
        self.isHidden = false
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 0.4) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.isHidden = true
        }
    }
    
    func removeFadeOut(duration: TimeInterval = 0.4) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.removeFromSuperview()
        }
    }
    
    func startRotate() {
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 1;
        rotationAnimation.isCumulative = true;
        rotationAnimation.repeatCount = .infinity;
        self.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func stopRotate() {
        self.layer.removeAnimation(forKey: "rotationAnimation")
    }
}
