/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

let MSG_GAME_DELAYED_TITLE = "Delay of Game"
let MSG_GAME_DELAYED_MESSAGE = "The game is being delayed.\nPlease try again later.\n"

let MSG_GAME_CANCELED_TITLE = "Cancellation of game"
let MSG_GAME_CANCELED_MESSAGE = "The game is canceled.\n\n"

let MSG_GAME_SCHEDULED_MESSAGE = "This program hasn\'t aired yet.\nYou can watch once it starts to air."
                            + "\n Please try again later."

let MSG_TITLE_NOTI = "Notification"
let MSG_TITLE_ERROR = "Error"

let MSG_NETWORK_PROBLEM = "There was a problem with the network."
let MSG_UNKNOWN_ERROR = "An unknow error has occureed. \n Please try again."
let MSG_LOAD_CONTENT_ERROR = "An unknown error has occurred while loading this content. \n Please try again."
let MSG_GAME_AIRED_YET = "This match hasn't been yet. \n The match will be aired alive. \n Please wait."

func LOGD(filePath: String = #file, line: Int = #line, funcName: String = #function, to:Any...) {
    #if DEBUG
    let now = NSDate()
    let app = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
    let fileName = URL(fileURLWithPath: filePath).lastPathComponent
    print("\(now.description) \(app)] ##### - [\(fileName) \(funcName)][Line \(line)] \(to)")
    #endif
}

class BaseViewController: UIViewController {
    @IBOutlet weak var vBlock: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var vGuid: UIView?
    @IBOutlet weak var vLandScapeGuide: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let blockView = vBlock {
            blockView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }
        configGuidView()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterForeground(noti:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterForeground(noti:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterBackground(noti:)),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(enterBackground(noti:)),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged(noti:)),
        name: UIDevice.orientationDidChangeNotification,
        object: UIDevice.current)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let guideView = self.vGuid {
            let key = NSStringFromClass(self.classForCoder)
            showGuideView(self.vGuid!, key: key)
        }
    }

    private func configGuidView() {
        if let guideView = vGuid {
            guideView.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        }
        if let guideView = vLandScapeGuide {
            guideView.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        }
    }
    
    internal func showGuideView(_ guidView: UIView, key: String) {
        if let isUserGuid: NSNumber = FDUtill.loadFromDefaultWithList(key: key) as? NSNumber {
            self.view.bringSubviewToFront(guidView)
            guidView.isHidden = isUserGuid as! Bool
        } else {
            guidView.isHidden = false
            self.view.bringSubviewToFront(guidView)
        }
    }
    
    open func initVariable() {
    }
    
    open func setupLayout() {
    }
        
    // MARK: Notification
    @objc internal func enterForeground(noti: NSNotification) {
    }
    
    @objc internal func enterBackground(noti: NSNotification) {
    }
    
    @objc internal func orientationChanged(noti: NSNotification) {        
    }
    
    // MARK: - Public Method
    public func showIndicator() {
        //LOGD(to: "\(#function)")
        DispatchQueue.main.async {
            if let view = self.vBlock {
                view.isHidden = false
                self.view.bringSubviewToFront(view)
            }
            if let aIndicator = self.indicator {
                aIndicator.isHidden = false
                aIndicator.startAnimating()
            }
        }
    }
    
    public func closeIndicator() {
        DispatchQueue.main.async {
            if let view = self.vBlock {
                view.isHidden = true
            }
            if let aIndicator = self.indicator {
                aIndicator.isHidden = true
                aIndicator.stopAnimating()
            }
        }
    }
    
    public func dismissController() {
        if let navi = self.navigationController {
            navi.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Alert Message
    public func showMessageAlert(message: String, title: String) {
        let controller = UIAlertController.init(title: title ,
                                                message: message ,
                                                preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) {(action) in
        }
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    // TODO: add block action
    //    func showMessageAlert(message: String, title: String) {
    //        let controller = UIAlertController.init(title: title ,
    //                                                message: message ,
    //                                                preferredStyle: UIAlertController.Style.alert)
    //        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) {(action) in
    //        }
    //        controller.addAction(okAction)
    //        self.present(controller, animated: true, completion: nil)
    //    }
    
    // MARK: - ETC
    public func logForOrient(orientation: UIDeviceOrientation, callby: String) {
        
        LOGD(to: "\(orientation.rawValue)")
        switch orientation {
        case .unknown, .portrait, .portraitUpsideDown:
            LOGD(to: "\(callby) >> portrait")
            break
        case .landscapeLeft:
            LOGD(to: "\(callby) >> landscapeLeft")
            break
        case  .landscapeRight:
            LOGD(to: "\(callby) >> landscapeRight")
            break
        default:
            LOGD(to: "\(callby) >> default")
            break
        }
    }
    
    @IBAction func guideCloseButtonTouchUpInside(_ sender: Any?) {
        let button = sender as! UIButton;
        if button.tag == 1 {
            let key = NSStringFromClass(self.classForCoder)
            closeGuideView(key)
        } else if button.tag == 2 {
            closeLandscapeGuideView("USER_GUIDE_LANDSCAPE")
        }
    }
    
    internal func closeGuideView(_ key: String){
        let value = true as NSNumber
        _ = FDUtill.updateUserDefault(key, value: value)
         if let guideView = vGuid {
             guideView.removeFadeOut()
         }
    }
    
    internal func closeLandscapeGuideView(_ key: String){
        let value = true as NSNumber
        _ = FDUtill.updateUserDefault(key, value: value)
         if let guideView = vLandScapeGuide {
             guideView.removeFadeOut()
         }
    }
}
