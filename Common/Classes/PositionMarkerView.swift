/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class PositionMarkerView: UIView {
    
    @IBOutlet var vImage: UIImageView!    
    private var scale: CGFloat = 1.0
    private var positionLenght: CGFloat = 0.0
        
    override func awakeFromNib() {
        self.isUserInteractionEnabled = false
    }
    /*
    override func draw(_ rect: CGRect) {
        
        let xPoint = self.frame.width/2;
        let yPoint = self.frame.height/2
        
        let xPath = UIBezierPath()
        xPath.move(to: CGPoint(x: xPoint - (10 * scale), y: yPoint - (5 * scale)))
        xPath.addLine(to: CGPoint(x: xPoint + (10 * scale), y: yPoint + (5 * scale)))
        xPath.move(to: CGPoint(x: xPoint - (10 * scale), y: yPoint + (5 * scale)))
        xPath.addLine(to: CGPoint(x: xPoint + (10 * scale), y: yPoint - (5 * scale)))
        xPath.lineWidth = scale
        UIColor.red.setStroke()
        xPath.stroke()
    }
    */
    private func setTargetPosition(_ length: CGFloat) {
        var rect: CGRect = vImage.frame
        if length > 0.0 {
            let ptY = (self.frame.height - rect.height) / 2
            rect.origin.y = ptY + length
        } else {
            //let ptX = (self.frame.width - rect.width) / 2
            let ptY = (self.frame.height - rect.height) / 2
            rect.origin.y = ptY
        }
        vImage.frame = rect
    }
            
    func setPostionLengthWithScale(_ scale: CGFloat, length: CGFloat) {
        self.scale = scale
        setTargetPosition(length)
    }
}
