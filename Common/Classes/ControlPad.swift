/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

enum DirectionToPad: Int8 {
    case unknown = 0
    case left = 1
    case right = 2
}

@objc protocol ControlPadDelegate {
    @objc optional func moveFromCenter(_ pad: ControlPad, point: CGPoint, direction: Int8)
    @objc optional func horizontalFromCenter(_ pad: ControlPad, point: CGPoint, direction: Int8)
    @objc optional func verticalFromCenter(_ pad: ControlPad, point: CGPoint, direction: Int8)
    
    @objc optional func moveBegin(_ pad: ControlPad)
    @objc optional func moveEnd(_ pad: ControlPad)
}

@objc protocol PadConterollerDataSource {
    
}

class ControlPad: BaseView {
    
    @IBOutlet var base: UIImageView!
    @IBOutlet var stick: UIImageView!
        @IBOutlet weak var delegate: ControlPadDelegate?
    
    private var pCenter: CGPoint = CGPoint.zero
    private var timer: Timer?
    private var currentTouches: Set<UITouch>?
    private let eventQueue: FDQueue = FDQueue<Int8>()
        
    private var _isEnable: Bool = true
    var isEnable: Bool {
        get { return _isEnable }
        set(newValue) {
            _isEnable = newValue
            stick.isHighlighted = !newValue
            if _isEnable == true {
                base.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.base.alpha = 1.0
                })
                
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.base.alpha = 0.0
                }) { (complete) in
                    self.base.isHidden = true
                }                
            }
        }
    }
    
    // MARK: - Life cycle
    deinit {
        timer?.invalidate()
        self.timer = nil
    }
    
    override func awakeFromNib() {
        initVariable()
    }
    
    override func initVariable() {
        pCenter.x = self.frame.width/2
        pCenter.y = self.frame.height/2
        base.isHidden = false
        self.isMultipleTouchEnabled = true
    }
    
    // MARK: - Private method
    private func touchEvent(_ touches: NSSet) {
        if touches.count != 1 { return }
        let touch = touches.anyObject() as! UITouch
        if touch.view != self { return }
        
        let touchPoint = touch.location(in: touch.view)
        var dir: CGPoint = CGPoint.zero
        var dtarget: CGPoint = CGPoint.zero
        var ret: CGPoint = CGPoint.zero
        
        dir.x = touchPoint.x - pCenter.x
        dir.y = touchPoint.y - pCenter.y
        let len = sqrt(dir.x * dir.x + dir.y * dir.y)
        let len_inv = 1.0 / len
        dtarget.x = dir.x * len_inv * 50
        let sign: Int = (dir.x < 0) ? -1 : (dir.x > 0) ? 1 : 0
        let retVal: CGFloat = min(abs(dir.x), abs(dtarget.x)) * CGFloat(sign)
        ret.x = round(retVal + (self.frame.width - stick.frame.width) / 2)
        stickMoveTo(ret)
        
        var horizon: DirectionToPad = .unknown
        let xValue = ret.x - 51
        if xValue > 0 {
            horizon = .right
        } else if ret.x - 51 < 0 {
            horizon = .left
        }
        let value = horizon.rawValue
        eventQueue.enqueue(value)
        
        guard let target = delegate else { return }
        if target.moveFromCenter != nil {
            target.moveFromCenter!(self, point: ret, direction: value)
        }
        if target.horizontalFromCenter != nil {
            target.horizontalFromCenter!(self, point: ret, direction: value)
        }
        if target.verticalFromCenter != nil {
            target.verticalFromCenter!(self, point: ret, direction: value)
        }
    }
    
    // MARK: Touch event
    private func stickMoveTo(_ point: CGPoint) {
        var rect = stick.frame
        rect.origin.x = point.x
        stick.frame = rect
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //if _isEnable == false { return }
        
        super.touchesBegan(touches, with: event)
        //stick.isHighlighted = true
        touchEvent(touches as NSSet)
        if timer == nil || timer!.isValid {
            self.timer = Timer.scheduledTimer(timeInterval: 0.04, target: self,
                                    selector: #selector(timerHandler(_:)), userInfo: nil, repeats: true)
        }        
        guard let target = delegate else { return }
        if target.moveBegin != nil {
            target.moveBegin!(self)
        }
    }
    
    @objc func timerHandler(_ sender: Timer) {
        if currentTouches != nil {
            //print("\(#function)")
            touchEvent(currentTouches! as NSSet)
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //if _isEnable == false { return }
        super.touchesBegan(touches, with: event)
        self.currentTouches = touches
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //if _isEnable == false { return }
        super.touchesBegan(touches, with: event)
        //stick.isHighlighted = false
        timer?.invalidate()
        timer = nil
        self.currentTouches = nil
        
        var dtarget: CGPoint = CGPoint.zero
        dtarget.x = (self.frame.width - stick.frame.width) / 2
        stickMoveTo(dtarget)
        
        guard let target = delegate else { return }
        if target.moveEnd != nil {
            target.moveEnd!(self)
        }
    }
    
    // MARK: - Public mehtod
    func getEvent() -> Int8? {
        return eventQueue.dequeue()
    }
    
    func clearEvent() {
        eventQueue.clear()
    }
}

