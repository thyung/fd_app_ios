/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class PlayerContainView: UIScrollView {
    
    private var toastTimer: Timer?
    
    private  var _isZoom: Bool = true
    var isZoom: Bool {
        get {
            return _isZoom
        }
        set(newVal) {
            _isZoom = newVal
            setZoom(newVal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initVariable()
    }
    
    func initVariable() {
        self.isScrollEnabled = false
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.minimumZoomScale = 1.0
        self.maximumZoomScale = 5.0
    }
    
    private func setZoom(_ isZoom: Bool) {
        self.minimumZoomScale = 1.0
        self.maximumZoomScale = isZoom ? 5.0 : 1.0
    }
    
    private func beginTimer() {
        invalidateTimer()
        sleep(UInt32(0.2))
        toastTimer = Timer.scheduledTimer(timeInterval: 3, target: self,
                                     selector: #selector(onTick(timer:)), userInfo: nil, repeats: false)
    }
    
    private func invalidateTimer() {
        if toastTimer != nil {
            toastTimer?.invalidate()
            toastTimer = nil
        }
    }
    
    @objc private func onTick(timer: Timer) {
        self.closeToastMessage()
    }
    
    func showToastMessage(_ message: String) {
        if message.count < 1 { return }
        
        var rect = self.frame
        var vBackground: UIView!
        var lbText: UILabel!
        let ivBackground: UIImageView!
        
        let font = UIFont.systemFont(ofSize: 14)
        let bString: NSString = message as NSString
        let size =  bString.size(withAttributes: [NSAttributedString.Key.font: font])
        LOGD(to: "Message size: \(size)");
        
        if let view = self.viewWithTag(9001) {
            vBackground = view
        } else {
            vBackground = UIView.init(frame: rect)
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(taptHandler(_:)))
            vBackground.addGestureRecognizer(tapRecognizer)
        }
        vBackground.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        vBackground.tag = 9001
        
        var ret = size.width / (rect.size.width - 64)
        if ret > 0 {
            ret = ceil(ret)
            rect.size.height = ret * size.height + 32
            rect.size.width = rect.width - 32
        }
        
        let bgImage = UIImage(named: "notice_bg")
        var frame = CGRect.init(x: 16, y: 16, width: rect.width, height: rect.height)
        
        if let imageView = self.viewWithTag(9002) {
            ivBackground = imageView as? UIImageView
            ivBackground.frame = frame
        } else {
            ivBackground = UIImageView.init(frame: frame)
            ivBackground.image = bgImage
            ivBackground.tag = 9002
            vBackground.addSubview(ivBackground)
        }
        
        if let labe = self.viewWithTag(9003) {
            lbText = labe as? UILabel
        } else {
            lbText = UILabel()
            lbText.numberOfLines = 3
            lbText.tag = 9003
        }
        frame.origin.x = frame.origin.x + 16
        frame.size.width = rect.width - 32
        lbText.frame = frame
        lbText.text = message
        
        vBackground.addSubview(lbText)
        self.addSubview(vBackground);
        vBackground.fadeIn()
        
        self.beginTimer()
    }
    
    @objc func taptHandler(_ recognizer: UITapGestureRecognizer) {
        closeToastMessage()
    }
    
    func closeToastMessage() {
        if let view = self.viewWithTag(9001) {
            view.removeFadeOut()
        }
    }
}
