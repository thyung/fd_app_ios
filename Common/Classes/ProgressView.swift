/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class FDThumbSlider : UISlider {
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        let unadjustedRect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        let offset: CGFloat = unadjustedRect.size.width / 2.0
        let minOffset = -offset
        let maxOffset = offset
        let value = minOffset + (maxOffset - minOffset) * CGFloat(value / (self.maximumValue - self.minimumValue))
        var origin = unadjustedRect.origin
        origin.x += value
        return CGRect(origin: origin, size: unadjustedRect.size)
    }
}

@objc protocol ProgressViewDelegate {
    @objc optional func sliderMoveBegin(_ slider: UISlider)
    @objc optional func sliderMoveEnd(_ slider: UISlider, time: Int, base: Float)
    @objc optional func progressFinish(_ slider: UISlider)
}

class ProgressView: UIView {
    
    @IBOutlet weak var delegate: ProgressViewDelegate?
    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var slider: FDThumbSlider!
    @IBOutlet weak var lbDuration: UILabel?
    @IBOutlet weak var btPause: UIButton!
    @IBOutlet weak var lbProgress: UILabel!
    @IBOutlet weak var btNow: UIButton?
    @IBOutlet weak var ivLive: UIImageView?    
    
    private var min: Int64 = 0
    private var max: Int64 = 0
    
    //private  var timer: Timer?
    //var livePauseTime: Int32 = 0
    
    var isTouch: Bool = false
    //private var base: Int64 = 3 * 60 * 1000
    @Atomic private var _duration: Int64 = 0
    var duration: Int64{
        get { return _duration }
        set(newValue){
            _duration = newValue
            if let label = lbDuration {
                DispatchQueue.main.async {
                    self.convertTime(newValue, label: label)
                }
            }
        }
    }
    
    private var _isLive: Bool = false
    var isLive: Bool {
        get {
            return _isLive
        }
        set(newValue) {
            _isLive = newValue
            if newValue == true {
                slider.value = 1.0
            } else {
                if let button = btNow { button.isHidden = true }
                if let imageView = ivLive { imageView.isHidden = true }
            }
        }
    }
    
    var atStartTime: Int64 = 0
    // for live
    func updateDuration(_ time: Int64) {
        let date = NSDate()
        let seconds = date.timeIntervalSince1970
        _duration = (Int64(seconds) - atStartTime) * 1000
        max = time
        min = time - _duration
    }
    
    func convertLocalToUTC(localTime: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let timeLocal = dateFormatter.date(from: localTime)

        if timeLocal != nil {
            dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")

            let timeUTC = dateFormatter.string(from: timeLocal!)
            return timeUTC
        }
        return nil
    }
        
    // MARK: - Life cycle
    override func awakeFromNib() {
        initVariable()
    }
    
    private func initVariable() {
        let image = UIImage.init()
        slider.setThumbImage(image, for: UIControl.State.normal)
        slider.addTarget(self, action: #selector(sliderTouchBegan(_:)), for: UIControl.Event.touchDown)
        slider.addTarget(self, action: #selector(sliderTouchEnd(_:)), for: UIControl.Event.touchUpInside)
        slider.addTarget(self, action: #selector(sliderTouchDrag(_:)), for: UIControl.Event.touchDragInside)
    }
    
    // MARK: - Private method
    private func convertTime(_ value: Int64, label: UILabel) {
        let sign = Int(value).signum()
        let seconds = abs(value)/1000
        var ms = NSMutableString.init(capacity: 8)
        let h = seconds / 3600
        let m = seconds / 60 % 60
        let s = seconds % 60

        if seconds < 0 {
            ms = "∞"
        } else {
            if sign < 0 { ms = "- " }
            if h > 0 { ms.appendFormat("%d:", h) }
            if m < 10 { ms.append("0") }
            ms.appendFormat("%d:", m)
            if s < 10 { ms.append("0") }
            ms.appendFormat("%d", s)
        }
        DispatchQueue.main.async { //[weak self] in
            label.text = ms as String
        }
    }
    
    private func beginTimer() {
//        invalidateTimer()
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self,
//                                     selector: #selector(onTick(timer:)), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
//        if timer != nil {
//            timer?.invalidate()
//            timer = nil
//        }
    }
    
    @objc private func onTick(timer: Timer) {
//        _duration = _duration - (1 * 1000)
//        if _duration > base * -1 {
//            convertTime(Int64(_duration), label: lbProgress)
//            let value = Float(_duration) / Float(base)
//            slider.value = 1 + value
//        }
    }
    
    @objc func sliderTouchBegan(_ sender: UISlider) {
        LOGD(to: "\(#function)")
        isTouch = true
        guard let target = delegate else { return }
        if target.sliderMoveBegin != nil {
            target.sliderMoveBegin?(slider)
        }
    }
    
    @objc func sliderTouchDrag(_ sender: UISlider) {
        //print("\(#function) value: \(sender.value)")
        if isLive == true {
            let time = (1 - slider.value) * Float(_duration)
            convertTime(Int64(time * -1), label: lbProgress)
        } else {
            let value = sender.value * Float(_duration)
            convertTime(Int64(value), label: lbProgress)
        }
    }
    
    @objc func sliderTouchEnd(_ sender: UISlider) {
        isTouch = false
        var value: Float = 0.0
        if isLive == true {
            let time = (1 - slider.value) * Float(_duration)
            convertTime(Int64(time * -1), label: lbProgress)
            lbProgress.isHidden = time != 0.0 ? false : true
            //value = Float(max) - (time / 1000)
            value = time
        } else {
            value = slider.value * Float(_duration)
        }
        
        print("\(#function) duration: \(_duration) value: \(Int(value)) time: \(String(describing: time))")
        guard let target = delegate else { return }
        if target.sliderMoveEnd != nil {
            target.sliderMoveEnd?(slider, time: Int(value), base: Float(_duration))
        }
    }
    
    // MARK: - Public method
    func setTimeShiftMode(_ isTimeShift: Bool) {
        if let button = btNow { button.isHidden = !isTimeShift }
        if let imageView = ivLive { imageView.isHidden = isTimeShift }
        if let label = lbProgress { label.isHidden = !isTimeShift }
        if isTimeShift == false {
            slider.value = 1
            invalidateTimer()
            _duration = 0
            convertTime(0, label: lbProgress)
        } else {
            //beginTimer()
        }
    }
    
    func updatePlayTime(_ time: Int64) {
        if isTouch == true { return }
        if time > 0 {
            if _isLive == true {
                //let ret = Float(_duration) - Float(time)
                //print(" update time: \(time)");
                let ret = Float(max) - Float(time)
                if ret <= 0 {
                    slider.value = 1
                    lbProgress.text = "00:00"
                    if let target = delegate, target.progressFinish != nil {
                        target.progressFinish!(slider)
                    }
                } else {
                    let temp = time - min
                    let value = Float(temp) / Float(_duration)
                    slider.value = value
                    let aTime = time - max //
                    convertTime(Int64(aTime), label: lbProgress)
                }
            } else {
                convertTime(time, label: lbProgress)
                let ret = Float(time) / Float(_duration)
                slider.value = ret
            }
        } else {
            slider.value = 0
            lbProgress.text = "00:00"
        }
    }
        
    func setPauseMode(_ isPause: Bool) {
        if isPause == true {
            invalidateTimer()
        }
    }
    
    func onMutilMode(_ isMuti: Bool) {
        ivBackground.isHidden = isMuti
        slider.isHidden = isMuti
        lbDuration?.isHidden = isMuti
        btPause.isHidden = isMuti
        lbProgress.isHidden = isMuti
        //btNow?.isHidden = isMuti
    }
}
