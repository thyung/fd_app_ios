/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class NextPageCell: UITableViewCell {
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var lbLoding: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func startIndicator() {
        if indicator != nil {
            indicator.isHidden = false
            indicator.startAnimating()
        }
    }
    
    public func stopIndicator() {
        if indicator != nil {
            indicator.isHidden = true
            indicator.stopAnimating()
        }
    }
}
