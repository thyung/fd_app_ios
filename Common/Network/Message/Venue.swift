/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class Venue: CMSRequest {
    
    override init() {
        super.init()
        cmd = String(describing: CMSRequest.self)
    }
    
    override func getUrl() -> URL? {
        var url: URL? = nil
        var aString = "\(self.baseUrl)/venue"
        if parasms != nil && parasms!.count > 0 {
            aString = "\(aString)?\(parasms ?? "")"
        }
        
        url = URL.init(string: aString)
        return url
    }

}
