/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

/*
 * "https://work.muteklab.com:50443/v1/content/{id}"
 */
class Content: CMSRequest {
    var id: String?  // {id}/status
    var event_id: String?
    var event_type: String?
    var content_type: String?
    var next: String?
    var isStatus: Bool = false
    
    override init() {
        super.init()
        cmd = String(describing: CMSRequest.self)
    }
        
    override func getUrl() -> URL? {
        var url: URL? = nil
        repeat {
            var aString = ""
            var eventId = ""
            var contentType = ""
            var eventType = ""
            var isAnd = false
            
            aString = "\(baseUrl)/content"
            if event_id != nil && event_id!.count > 0  {
                eventId = "event_id=\(event_id ?? "")"
            }
            if content_type != nil && content_type!.count > 0 {
                contentType = "content_type=\(content_type!)"
            }
            if event_type != nil && event_type!.count > 0 {
                eventType = "event_type=\(event_type!)"
            }
            if eventId.count > 0     {
                aString = "\(aString)?\(eventId)"
                isAnd = true
            }
            if contentType.count > 0 {
                aString =  isAnd ? "\(aString)&\(contentType)" : "\(aString)?\(contentType)"
                isAnd = true
            }
            if eventType.count > 0 {
                aString = isAnd ? "\(aString)&\(eventType)" : "\(aString)?\(eventType)"
            }
            
            if id != nil && id!.count > 0 {
                aString = isStatus ? "\(baseUrl)/content/\(id ?? "")/status" : "\(baseUrl)/content/\(id ?? "")"
            }
            if next != nil && next!.count > 0 {
                aString = "\(baseUrl)/content?page=\(next ?? "")"
            }
            if parasms != nil && parasms!.count > 0 {
                aString = "\(baseUrl)/content?\(parasms ?? "")"
            }
            url = URL.init(string: aString)
        } while(false)
        
        return url
    }
    
    /*
     * https://work.muteklab.com:50443/v1/content/{id}/status
     */
//    public func makeStatusRequest(_ statusID: String?) -> URLRequest? {
//        var searchID = statusID
//        if searchID == nil || searchID!.count < 1 {
//            if id == nil || id!.count < 1 {
//                LOGD(to: "Invalid id, it is required to search.")
//                return nil
//            }
//            searchID = id
//        }
//        
//        let aString = "\(baseUrl)/content/\(searchID ?? "")/status"
//        guard let url = URL.init(string: aString) else { return nil }
//        LOGD(to: "\(#function) url: \(url.absoluteString)")
//        
//        var request: NSMutableURLRequest?
//        request = NSMutableURLRequest.init(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
//        request?.addValue("application/json", forHTTPHeaderField: "Accept")
//        request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request?.httpMethod = "GET"
//        
//        return request! as URLRequest;
//    }
}
