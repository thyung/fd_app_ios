/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

/*
 * https://work.muteklab.com:50443/v1/?venue_id=123?event?event_type=vod
 */
class Event: CMSRequest {
    
    var id: String? //status
    var venue_id: String?
    var event_type: String?    
    var next: String?
    var isStatus: Bool = false
    
    override init() {
        super.init()
        cmd = String(describing: Event.self)
    }
    
    override func getUrl() -> URL? {
        var url: URL? = nil
        repeat {
            var venueId = ""
            var type = ""
            var isAnd = false
            
            if venue_id != nil && venue_id!.count > 0 {
                venueId = "venue_id=\(venue_id!)"
            }
            if event_type != nil && event_type!.count > 0 {
                type = "event_type=\(event_type!)"
            }
            var aString = "\(self.baseUrl)/event"
            if venueId.count > 0 {
                aString = "\(aString)?\(venueId)"
                isAnd = true
            }
            if type.count > 0 {
                aString = isAnd ? "\(aString)&\(type)" : "\(aString)?\(type)"
            }
            
            if next != nil && next!.count > 0 {
                aString = "\(baseUrl)/event?page=\(next ?? "")"
            }
            if parasms != nil && parasms!.count > 0 {
                aString = "\(baseUrl)/event?\(parasms ?? "")"
            }
            if id != nil && id!.count > 0 {
                // TODO: 임시
                aString = isStatus ? "\(baseUrl)/event/\(id ?? "")/status" : "\(baseUrl)/event?\(id ?? "")"
                //aString = isStatus ? "\(baseUrl)/event/\(id ?? "")" : "\(baseUrl)/event?\(id ?? "")"
            }
            
            url = URL.init(string: aString)
        } while(false)
        
        return url
    }
}
    
