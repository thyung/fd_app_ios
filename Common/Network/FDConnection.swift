/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

/* AWS Release */
let BASE_URL = "https://gsw.kiosk.4dreplay.io/v4"
let IMAGE_URL = "https://gsw.kiosk.4dreplay.io"

/* Internal */
//let BASE_URL = "https://awsko.kiosk.4dreplay.io/v4"
//let IMAGE_URL = "https://awsko.kiosk.4dreplay.io"

/* Studio c */
//let BASE_URL = "http://10.82.14.253/v4"
//let IMAGE_URL = "http://10.82.14.253"

/* Test Room QA */
//let BASE_URL = "http://192.168.0.4/v4"
//let IMAGE_URL = "http://192.168.0.4"

/* MCC studio b */
//let BASE_URL = "http://192.168.0.101/v4"
//let IMAGE_URL = "http://192.168.0.101"

typealias ResponseBlock = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

class FDConnection: NSObject {

    class func dataTaskWithRequest(_ request: URLRequest, block: ResponseBlock?) {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = true
        config.requestCachePolicy = .reloadIgnoringCacheData
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if block != nil {
                block!(data, response, error)
            }
        })
        task.resume()
    }
    
    class func dataTaskWithRequest(_ config: URLSessionConfiguration, request: URLRequest, block: ResponseBlock?) {
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if block != nil {
                block!(data, response, error)
            }
        })
        task.resume()
    }
    
    class func requestImageWithTarget(_ target: UIImageView!, url: String?) {
        LOGD(to: "\(String(describing: url))")
        if url != nil {
            let pullUrl = IMAGE_URL + url!
            guard let aUrl = URL(string: pullUrl) else { return }
            
            let indicator = UIActivityIndicatorView(style: .medium)
            indicator.color = UIColor.white
            indicator.center = target!.center
            target?.addSubview(indicator)
            indicator.startAnimating()

            URLSession.shared.dataTask(with: aUrl) { data, response, error in
                DispatchQueue.main.async {
                    indicator.stopAnimating()
                    indicator.removeFromSuperview()
                }
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let aData = data, error == nil,
                    let image = UIImage(data: aData)
                else { return }
                DispatchQueue.main.async() {
                    target.image = image
                }
            }.resume()
        }
    }
}

var __mark: Int = 0

//const val CMS_URL : String = "http://192.168.0.4/"
//let imageUrl = "http://192.168.0.4"
//let imageUrl = "https://awsko.kiosk.4dreplay.io"

class CMSRequest: NSObject {
    
    internal var baseUrl: String = BASE_URL
    var request: URLRequest?
    var cmd: String?
    var parasms: String? /*search params*/
    
    override init() {
        super.init()
        self.initVariable()
    }
    
    private func initVariable() {
        __mark += 1        
    }
    
    open func getUrl() -> URL? {
        return nil
    }
    
    //MARK: - Public method
    public func makeRequest() -> URLRequest? {
        
        guard let url = self.getUrl() else { return nil }
        
        request = URLRequest.init(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request?.addValue("application/json", forHTTPHeaderField: "Accept")
        request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.httpMethod = "GET"
                
        return request! as URLRequest;
    }
}

class  LSRequest: NSObject {
    private var processID: Int = __mark
    internal var baseUrl: String?
    
    var request: URLRequest?
    var cmd: String?
    var type: String?
    var id: String?
        
    init(withAddress address: String?) {
        super.init()
        baseUrl = address
        self.initVariable()
    }
    
    private func initVariable() {
        __mark += 1
    }
    
    open func getUrl() -> URL? {
        var url: URL? = nil
        repeat {
            var paramId = ""
            var strType = ""
            
            if id != nil && id!.count > 0 {
                paramId = "id=\(id!)"
            }
            if type != nil && type!.count > 0 {
                strType = "type=\(type!)"
            }
            
            if baseUrl != nil {
                var aString = "\(baseUrl ?? "")/service/4dss/url"
                if strType.count > 0 {
                    aString = "\(aString)?\(strType)"
                }
                if paramId.count > 0 {
                    aString = "\(aString)&\(paramId)"
                }
                url = URL.init(string: aString)
            }
        } while(false)
        
        return url
    }
    
    public func makeRequest() -> URLRequest? {
        
        guard let url = self.getUrl() else { return nil }
        
        request = URLRequest.init(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request?.addValue("application/json", forHTTPHeaderField: "Accept")
        request?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request?.httpMethod = "GET"
                
        return request! as URLRequest;
    }
    
}

