/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

@objc protocol BirdViewDelegate {
    @objc optional func swipeMultiview(_ direct: Int) // 1: right -1: left
    @objc optional func selectCameraPosition(index: Int)
}

class BirdView: BaseView {
    
    let __MAX = 42

    @IBOutlet weak var ivBackground: UIImageView!
    @IBOutlet weak var ivRail: UIImageView!
    @IBOutlet weak var btRestart: UIButton!
    @IBOutlet weak var btToggle: UIButton?
    
    internal var closeComplete: (() -> Void)?
    private var isOpened = false
    private var distanceX: CGFloat = 0.0
    private var railImages: [UIImage]! = [] // temporary
    private var current = 0
    private var index = 0
    private var isAutoMode = false
        
    weak var delegate: BirdViewDelegate?
    var streamUrl: String?
    var fdPlayer: FDLivePlayer!
    //var leftIndex = 0    
    //var totalCount = 0
    var leftIndex = 41
    var totalCount = 80
            
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
        setupBirdViewPlayer()
    }
    
    override func setupLayout() {
        let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(panRecognized(_: )))
        self.addGestureRecognizer(panRecognizer)
        for i in 0..<__MAX  {
            if let image = UIImage(named: "rail_\(i+1)") {
                railImages?.append(image)
            }
        }
    }
    
    private func setupBirdViewPlayer() {
        /* FDLPlayer */
        fdPlayer = FDLivePlayer(delegate: self) //delegate?
        fdPlayer.delegate = self
        fdPlayer.playerView.frame = self.bounds
        fdPlayer.playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.insertSubview(fdPlayer.playerView, at: 0)
    }
    
    @objc func panRecognized(_ recognizer: UIPanGestureRecognizer) {
        let distance = recognizer.translation(in: self)
        if recognizer.state == UIGestureRecognizer.State.changed {
            recognizer.cancelsTouchesInView = false
            let valueX = abs(distance.x)
            let valueY = abs(distance.y)
            if valueX > valueY {
                panGestureSwipe(distance: distance)
            }
        } else if recognizer.state == UIGestureRecognizer.State.ended {
            LOGD(to: "\(#function) UIGestureRecognizerStateEnded")
            distanceX = 0.0;
        }
    }
    
    private func panGestureSwipe(distance: CGPoint) {
        let value = distance.x - distanceX
        let absolute = abs(value)
        if value > 0 {
            if absolute > 10 {
                //increaseChannel()
                index += 1
            }
        } else {
            if absolute > 10 {
                //decreaseChannel()
                index -= 1
            }
        }
        updateCameraPosition(index)
        distanceX = distance.x
    }
    
    //MARK: Open Birdview Stream ()
    func setupStream(_ baseUrl: String?, channel: Channel?) -> String? {
        var retUrl: String? = nil
        if var strUrl = baseUrl, let item = channel, let group = channel?.id {
            strUrl = strUrl + "&group=\(group)"
            if item.subchannels != nil && item.subchannels!.count > 1 {
                var aUrl = strUrl
                if let target = item.subchannels?[0]["index"] {
                  aUrl = aUrl + "&target=\(target)"
                }
                retUrl = aUrl
            } else {
                retUrl = strUrl
            }
        }        
        // TODO: for working
        retUrl = "rtsp://10.82.5.99/mbc_test03.4ds?type=vod"
        
        return retUrl
    }
        
    func openStream(_ baseUrl: String?, channel: Channel?) -> String? {
        let retUrl = setupStream(baseUrl, channel: channel)
        _ = onOpen(retUrl)
        return retUrl
    }
    
    private func onOpen(_ url: String?) -> String? {
        var aUrl: String? = nil
        if url == nil {
            aUrl = streamUrl
        } else {
            aUrl = url
            self.streamUrl = url
        }
        
        if let strUrl = aUrl {
            DispatchQueue.main.async { self.startIndicator() }
            var ret = -1
            ret = Int(fdPlayer.streamOpen(strUrl, isTCP: true, isHWAccel: true));
            LOGD(to: "Birdview rtsp address: \(String(describing: strUrl)) ret: \(ret)")
            if ret != 0 {
                DispatchQueue.main.async { self.stopIndicator() }
            }
        } else {
            DispatchQueue.main.async { self.stopIndicator() }
        }
        return aUrl
    }
    
    public func close() {
        fdPlayer.streamClose()
    }
    
    public func pause() {
        fdPlayer.pause()
    }
    
    public func playToNow() {
        isOpened ? (_ = fdPlayer.playToNow()) : (_ = onOpen(nil))
    }
    
    public func play() {
        fdPlayer.play()
    }
    
    public func startIndicator() {
        if indicator != nil {
            indicator!.isHidden = false
            indicator!.startAnimating()
        }
    }
    
    public func stopIndicator() {
        if indicator != nil {
            indicator!.isHidden = true
            indicator!.stopAnimating()
        }
    }
    
    // MARK: - Rail
    func increaseChannel() {
        index += 1
        if index > (__MAX - 1) {
            index = 0
        }
        print("\(#function) index: \(index)")
        ivRail.image = railImages[index]
    }
    
    func decreaseChannel() {
        index -= 1
        if index < 0 {
            index = __MAX - 1
        }
        print("\(#function) index: \(index)")
        ivRail.image = railImages[index]
    }
    
    func updateCameraPosition(_ channel: Int) {
        if channel < 1 || leftIndex < 4 || totalCount < 0 || totalCount < leftIndex {
            return
        }
        
        var index = current
        switch channel {
        case 1:
            index = 0
            break
        case 2..<(leftIndex - 2):
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index < 1 { index = 1 }
            let lastIndex = leftIndex - 2
            if channel < lastIndex && index >= 21 {
                index = 20
            }
            break
        case leftIndex - 1:
            index = 21
            break
        case leftIndex..<totalCount:
            let interval:Float = Float(totalCount) / 42.0
            index = Int(Float(channel) / interval) + 1
            if index > railImages.count - 2 {
                index = railImages.count - 2
            }
            break
        case totalCount:
            index = railImages.count - 1
            break
        default:
            if totalCount < channel {
                index = railImages.count - 1
            }
            break
        }
        
        DispatchQueue.main.async {
            self.ivRail.image = self.railImages[index]
            self.current = index
            print("\(#function) channel: \(channel) index: \(index)")
            print("\(#function)>>>>>> \(String(describing: self.ivRail.image))")
        }
    }
    
    func onAutoTrakingMode(_ isMode: Bool) {
        isAutoMode = isMode
        ivRail.isHidden = isMode
        if let button = btToggle {
            button.isSelected = isMode
            button.isHidden = isMode
        }
    }
       
    // MARK: - Button Action
    @IBAction func cameraButtonTouchUpInside(_ sender: Any?) {
        if isAutoMode == true { return }
        //1  6, 11, 16, 21
        //22, 27, 32, 37, 42
        var channel = 1
        let interval: Float = Float(totalCount) / 8.0
        print("\(#function) tatal: \(totalCount) interval: \(interval)")
        if let tag = (sender as? UIButton)?.tag {
            current = tag - 1
            self.ivRail.image = railImages[tag - 1]
            switch tag {
            case 1:
                channel = 1
                break
            case 6..<21:
                let idx = tag / 5
                channel = Int(Float(idx) * interval)
                break
            case 21:
                channel = leftIndex - 1
                break
            case 22:
                channel = leftIndex
                break
            case 23..<42:
                let idx = tag / 5 - 4
                print("\(#function)>>>>>> idx: \(idx)")
                channel = leftIndex + Int(Float(idx) * interval)
                break
            case 42:
                channel = totalCount
                break
            default:
                break
            }
        }
        if let target = delegate,
           target.selectCameraPosition != nil {
            target.selectCameraPosition?(index: channel)
        }
    }
        
    @IBAction func toggleButtonTouchUpInside(_ sender: Any?) {
        if let button = sender as? UIButton {
            button.isSelected = !button.isSelected
            ivRail.isHidden = button.isSelected
        }
    }
    
    @IBAction func reStartButtonTouchUpInside(_ sender: Any?) {
        _ = onOpen(nil)
    }
}


// MARK: - FDLivePlayer Delegate
extension BirdView:  FDPlayerDelegate {
    func getCurrentPlayInfo(_ player:FDLivePlayer, channel: Int32, frame: Int32,
                            frameCycle: Int32, time: Int32, utc: String, type: Int32) {
            //LOGD(to: "channel: \(channel), frame: \(frame) cycle: \(frameCycle) time: \(time) tuc: \(utc)")
    }
    
    func getVideoStreamInfo(_ player:FDLivePlayer, width: Int32, height: Int32, duration: Int32,
                            videoCodec: String, audioCodec: String) {
        //LOGD(to: "\(#function) width: \(width) height: \(height) duration: \(duration).")
    }
    
    func getStart(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
    }
    
    func getStop(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
        if code == 0 {
            if let handler = self.closeComplete {
                handler()
            }
            DispatchQueue.main.async {
                switch code {
                case Int32(FD_ERR_ENG_2200.rawValue),
                     Int32(FD_ERR_ENG_2201.rawValue),
                     Int32(FD_ERR_ENG_2203.rawValue),
                     Int32(FD_ERR_ENG_2204.rawValue),
                     Int32(FD_ERR_ENG_2205.rawValue):
                    self.btRestart.isHidden = false
                    break
                default:
                    self.btRestart.isHidden = true
                    break
                }
                self.stopIndicator()
            }
        }
    }
    
    func getPlay(_ player:FDLivePlayer) {
        DispatchQueue.main.async {
            self.btRestart.isHidden = true
            self.ivBackground.isHidden = true
            self.stopIndicator()
            self.isOpened = true
        }
    }
    
    func getPause(_ player:FDLivePlayer) {
        LOGD(to: "\(#function)")
    }
    
    func getPlayDone(_ player:FDLivePlayer) {
        LOGD(to: "\(#function)")
    }
    
    func getError(_ player:FDLivePlayer, code: Int32, message: String) {
        LOGD(to: "\(#function) code: \(code) message: \(message)")
        switch code {
        case Int32(FD_ERR_ENG_2300.rawValue), Int32(FD_ERR_ENG_2301.rawValue):
            DispatchQueue.main.async {
                self.btRestart.isHidden = false
                self.ivBackground.isHidden = false
            }
            break
        default:
            break
        }
        DispatchQueue.main.async {
            self.stopIndicator()
            self.isOpened = false
        }
    }
}
