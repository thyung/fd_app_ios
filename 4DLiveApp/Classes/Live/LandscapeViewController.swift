/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class LandscapeViewController: PlayerController {

    @IBOutlet weak var vPlay: PlayerContainView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescript: UILabel!
    @IBOutlet weak var playerViewTraillingConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerViewLeadingConstraint: NSLayoutConstraint!
    
    var content: [String : AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()        
        if let info: [String : AnyObject] = content {
            configLandScapeInfo(info)
#if true
            if let baseUrl = info["stream_url"] as? String {
                let mainStream = self.getDefaultStreamUrl(baseUrl, content: self.content!)
                self.showIndicator()
                self.streamOpen(mainStream)
            }
#else
            if let url = info["ls_url"] as? String,
               let id = info["file_id"] as? String,
               let type = info["content_type"] as? String {
                self.didRequestCompletion = {items in
                    if items != nil {
                        if let baseUrl = items?["value"] {
                            let mainStream = self.getDefaultStreamUrl(baseUrl, content: self.content!)
                            self.streamOpen(mainStream)
                        }
                    }
                }
                self.requestStreamUrl(url, id: id, type: type)
            }
#endif
        }
    }
    
    override func setupLayout() {
        self.navigationController?.navigationBar.isHidden = true
        // TODO: scale 수정할것
        
        let width = getPlayerWidth(self.view.frame.width)
        let constant = (self.view.frame.height - width - 88) / 2
        playerViewLeadingConstraint.constant = constant
        playerViewTraillingConstraint.constant = constant
        
        self.setupPlayer(vPlay, rect: vPlay.bounds)
        vPlay.delegate = self
        
        AppUtility.lockOrientation(.landscapeRight)
        let value = UIDeviceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    private func configLandScapeInfo(_ event: [String : AnyObject]) {
        lbTitle.text = event["title"] as? String
        let descript = event["description"] as? String
        let scheduel = event["scheduled_at"] as? String
        var vs = ""
        if let metaInfo = event["event_meta_info"] {
            var home = ""
            var away = ""
            if let aString = metaInfo["home_team_name"] as? String {
                home = aString
            }
            if let aString = metaInfo["away_team_name"] as? String {
                away = aString
            }
            vs = "\(home) vs \(away)"
        }
        lbDescript.text = "\(descript ?? "") | \(vs) | \(scheduel ?? "")"
    }
    
    private func getDefaultStreamUrl(_ baseUrl: String!, content: [String : AnyObject]) -> String? {
        var streamUrl = baseUrl
        if let groupInfo = content["group_info"] as? [String : AnyObject] {
            if let groups: [[String : AnyObject]] = groupInfo["groups"] as? [[String : AnyObject]] {
                if let defaultID = groupInfo["default_group_id"] as? String,
                   let defaultChannel = (groups.first)?["default_channel_id"] {
                    streamUrl = "\(streamUrl!)&group=\(defaultID)&target=\(defaultChannel)"
                }
            }
        }
        
        return streamUrl
    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any?) {
        if self.fdPlayer != nil {
            self.fdPlayer.streamClose()
            self.fdPlayer = nil
        }
        
        AppUtility.lockOrientation(.portrait)
        let value = UIDeviceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        self.dismissController()
    }
}
