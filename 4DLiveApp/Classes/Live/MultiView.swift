/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class MultiItemView: BaseView {
    
    /*
     TODO: 오른쪽 큰거는 피디뷰 바뀔때 왼쪽, 오른쪽  판단해서 채널 변환 해줘야한다.
     pdView 정보에 카메라 인덱스 정보가 있다 해당 데이터 기준으로 왼/오른쪽 판단해서 처리하면된다.        
     */
    
    @IBOutlet var btRestart: UIButton!
    
    var fdPlayer: FDLivePlayer!
    var streamUrl: String?
    var isMutilView: Bool = false
    var screenScale: CGFloat = 1.0
    var isCrop = false
    
    private var isOpened = false
    private var wResolution: Int32 = 0
    private var hResolution: Int32 = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override func setupLayout() {
        screenScale = UIScreen.main.scale
        
        fdPlayer = FDLivePlayer(delegate: self)
        fdPlayer.delegate = self
        fdPlayer.playerView.frame = self.bounds
        fdPlayer.playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.insertSubview(fdPlayer.playerView, at: 0)
    }
        
    // MARK: Open Stream ()
    func openStream(_ url: String?) {
        var aUrl: String? = nil
        if url == nil {
            aUrl = streamUrl
        } else {
            aUrl = url
            self.streamUrl = url
        }
        
        if let strUrl = aUrl {
            DispatchQueue.main.async { self.showIndicator() }
            var ret = -1
            ret = Int(fdPlayer.streamOpen(strUrl, isTCP: true, isHWAccel: true));
            LOGD(to: "Multive rtsp address: \(String(describing: strUrl)) ret: \(ret)")
            print("Multive rtsp address: \(String(describing: strUrl)) ret: \(ret)")
            if ret != 0 {
                DispatchQueue.main.async { self.closeIndicator() }
            }
        } else {
            DispatchQueue.main.async { self.closeIndicator() }
        }
    }
    
    public func close() {
        fdPlayer.streamClose()
        //fdPlayer = nil
    }
    
    public func pause() {
        fdPlayer.pause()
    }
    
    public func playToNow() {
        //fdPlayer.playToNow()
        isOpened ? (_ = fdPlayer.playToNow()) : openStream(nil)
    }
    
    public func play() {
        fdPlayer.play()
    }
    
    public func configViewPort() {
        let ret = getDisplayRect()
        self.fdPlayer.setDisplay(ret.rect, scale: 1.0)
    }
    
    // MARK: - Private Method
    private func getDisplayRect() -> (size: CGSize, rect: CGRect) {
        let width = self.frame.width
        let screenWidth: CGFloat = (width * 3 * 16) / 9
        let ptX: CGFloat = screenWidth / 4
        let ptY: CGFloat = 0.0
        let rect = CGRect(x: ptX, y: ptY, width: screenWidth, height: 375 * 3);
        print("\(#function)>>>>>> \(rect)");
        let size = CGSize(width: screenWidth, height: 375.0)
        
        return (size, rect)
    }
    
    // MARK - Button Action
    @IBAction func reStartButtonTouchUpInside(_ sender: Any?) {
        if fdPlayer.state == FD_STATE_PAUSE {
            fdPlayer.playToNow()
        } else {
            openStream(streamUrl)
        }
    }
}

// MARK: - FDLivePlayer Delegate
extension MultiItemView:  FDPlayerDelegate {
    func getCurrentPlayInfo(_ player:FDLivePlayer, channel: Int32, frame: Int32,
                            frameCycle: Int32, time: Int32, utc: String, type: Int32) {
            //LOGD(to: "channel: \(channel), frame: \(frame) cycle: \(frameCycle) time: \(time) tuc: \(utc)")
    }
    
    func getVideoStreamInfo(_ player:FDLivePlayer, width: Int32, height: Int32, duration: Int32,
                            videoCodec: String, audioCodec: String) {
        //LOGD(to: "\(#function) width: \(width) height: \(height) duration: \(duration).")
        self.wResolution = width
        self.hResolution = height
        
    }
    
    func getStart(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
                
        #if false
        // for test
        DispatchQueue.main.async {
            let view = UIView.init(frame: CGRect.init(x: 0,
                                                      y: self.bounds.height / 2,
                                                      width: self.bounds.width,
                                                      height: 1))
            view.backgroundColor = .red
            self.addSubview(view)
            
            let view02 = UIView.init(frame: CGRect.init(x: self.bounds.width / 2,
                                                        y: 1 ,
                                                        width: 1,
                                                        height: self.bounds.height))
            view02.backgroundColor = .red
            self.addSubview(view02)
        }
        #endif
    }
    
    func getStop(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
        if code == 0 {
            DispatchQueue.main.async {
                switch code {
                case Int32(FD_ERR_ENG_2200.rawValue),
                     Int32(FD_ERR_ENG_2201.rawValue),
                     Int32(FD_ERR_ENG_2203.rawValue),
                     Int32(FD_ERR_ENG_2204.rawValue),
                     Int32(FD_ERR_ENG_2205.rawValue):
                    self.btRestart.isHidden = false
                    break
                default:
                    self.btRestart.isHidden = true
                    break
                }
                self.closeIndicator()
            }
        }
    }
    
    func getPlay(_ player:FDLivePlayer) {
        DispatchQueue.main.async {
            self.btRestart.isHidden = true
            self.closeIndicator()
            self.isOpened = true
        }
    }
    
    func getPause(_ player:FDLivePlayer) {
    }
    
    func getPlayDone(_ player:FDLivePlayer) {
        LOGD(to: "\(#function)")
    }
    
    func getError(_ player:FDLivePlayer, code: Int32, message: String) {
        LOGD(to: "\(#function) code: \(code) message: \(message)")
        if code > 2000 && code < 3000 {
            DispatchQueue.main.async {
                self.btRestart.isHidden = false
            }
        }
        DispatchQueue.main.async {
            self.closeIndicator()
            self.showMessageAlert(message: message, title: "Error")
        }
    }
}

@objc protocol MultiViewDelegate: AnyObject {
    @objc optional func selectedMultiView(_ view: MultiItemView, index: Int, streamUrl: String?)
}

class MultiView: BaseView {
    
    @IBOutlet weak var vFisrt: MultiItemView!
    @IBOutlet weak var vSecond: MultiItemView!
    @IBOutlet weak var vThird: MultiItemView!
    @IBOutlet weak var delegate: MultiViewDelegate?
    @IBOutlet weak var vSecondcenterXConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //setupLayout()
        
        if let constraint = vSecondcenterXConstraint {
            constraint.constant = UIScreen.main.bounds.width / 4
        }
    }
    
    internal override func setupLayout() {
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        vFisrt.tag = 0
        vFisrt.addGestureRecognizer(tapGesture)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        vSecond.tag = 1
        vSecond.addGestureRecognizer(tapGesture)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        vThird.tag = 2
        vThird.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapHandler(_ sender: UITapGestureRecognizer) {
        if let view: MultiItemView = sender.view as? MultiItemView {
            switch view.tag {
            case 0:
                break
            case 1:
                break
            case 2:
                break
            case 3:
                break
            default:
                break
            }
            if let target = delegate {
                if target.selectedMultiView != nil {
                    target.selectedMultiView?(view , index: view.tag, streamUrl: view.streamUrl)
                }
            }
        }
    }
    
    func setupStream(_ baseUrl: String?, channel: Channel?) {
        if var strUrl = baseUrl, let item = channel, let group = channel?.id {
            strUrl = strUrl + "&group=\(group)"
            //strUrl = strUrl + "&group=2" // TODO: for working
            if item.subchannels != nil && item.subchannels!.count > 2 {
                var aUrl = strUrl
                if let target = item.subchannels?[0]["id"] {
                  aUrl = aUrl + "&target=\(target)"
                }
                vFisrt.streamUrl = aUrl
                LOGD(to: "\(#function) url - 1 \(aUrl)");
                
                aUrl = strUrl
                if let target = item.subchannels?[1]["id"] {
                  aUrl = aUrl + "&target=\(target)"
                }
                vSecond.streamUrl = aUrl
                LOGD(to: "\(#function)url - 2 \(aUrl)");
                
                aUrl = strUrl
                if let target = item.subchannels?[2]["id"] {
                  aUrl = aUrl + "&target=\(target)"
                }
                vThird.streamUrl = aUrl
                LOGD(to: "\(#function)url -  3 \(aUrl)");
            } else {
                vFisrt.streamUrl = strUrl
                vSecond.streamUrl = strUrl
                vThird.streamUrl = strUrl
            }
        }
    }
    
    func openStream(_ baseUrl: String?, channel: Channel?) {
        if baseUrl != nil && channel != nil {
            setupStream(baseUrl, channel: channel)
        } else {
            vFisrt.openStream(nil)
            vSecond.openStream(nil)
            vThird.openStream(nil)
        }
    }
    
    func close() {
        vFisrt.close()
        vSecond.close()
        vThird.close()
    }
    
    public func pause() {
        vFisrt.pause()
        vSecond.pause()
        vThird.pause()
    }
    
    public func playToNow() {
        vFisrt.playToNow()
        vSecond.playToNow()
        vThird.playToNow()
    }
    
    public func play() {
        vFisrt.play()
        vSecond.play()
        vThird.play()
    }
}
