/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class Channel: NSObject {
//    class SubChannel: NSObject {
//        var id: String?
//        var index: String?
//        var privateIP: String?
//        var privatePort: String?
//    }
    
    var id: String?
    var name: String?
    var defaultChannel: String?
    var subchannels: [[String: AnyObject]]?
    var extrachannels: [[String: AnyObject]]?
    
    private var info: [String: AnyObject]?
    
    override init() {
        super.init()
    }
    
    init(group: [String: AnyObject]?) {
        super.init()
        self.parsing(group: group)
    }
    
    func parsing(group: [String: AnyObject]?) {
        if let info = group {
            if let ret = info["id"] as? String {
                self.id = ret
            }
            if let ret = info["name"] as? String {
                self.name = ret
            }
            if let ret = info["default_channel_id"] as? String {
                self.defaultChannel = ret
            }
            if let ret = info["subchannels"] as? [[String: AnyObject]] {
                self.subchannels = ret
            } 
            if let ret = info["extrachannels"] as? [[String: AnyObject]] {
                self.extrachannels = ret
            }
        }
    }
}

class LiveViewController: PlayerController {

    @IBOutlet weak var vTop: UIView!
    @IBOutlet weak var lbTitleDescript: UILabel!
    @IBOutlet weak var ivLeft: UIImageView!
    @IBOutlet weak var lbLeftScore: UILabel!
    @IBOutlet weak var lbLeftName: UILabel!
    @IBOutlet weak var ivRight: UIImageView!
    @IBOutlet weak var lbRightScore: UILabel!
    @IBOutlet weak var lbRightName: UILabel!
    @IBOutlet weak var lbQuater: UILabel!    
    
    @IBOutlet weak var vPosition: PositionMarkerView!
    @IBOutlet weak var vLandScapeMuti: MultiView!
    @IBOutlet weak var btPipClose: UIButton!
    @IBOutlet weak var btAutoSwitch: SwieeftSwitchButton!    
    
    @IBOutlet weak var btBird: UIButton!
    @IBOutlet weak var btMulti: UIButton!
    @IBOutlet weak var vBird: BirdView!
    @IBOutlet weak var vMulti: MultiView!
    @IBOutlet weak var vMidContain: UIView!
    
    @IBOutlet weak var vSliding: UIView!
    @IBOutlet weak var vInterval: UIView!
    @IBOutlet weak var btReload: UIButton!
    @IBOutlet weak var vTable: UITableView!
        
    @IBOutlet weak var lbTitle: UILabel! // for landscape
    @IBOutlet weak var lbDescript: UILabel!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btPipShow: UIButton!
    @IBOutlet weak var vPipContain: BirdView!
    
    @IBOutlet weak var upSlideConstraint: NSLayoutConstraint!
    @IBOutlet weak var downSlideConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerHeightConstraint: NSLayoutConstraint!
                
    private var contentList: [[String : AnyObject]]?
    private var contents: [[String : AnyObject]]?
    private var isRequest = false
    
    private var channelCount = 0

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.fdPlayer.streamClose()
        self.vBird.close()
        self.vMulti.close()
    }
    
    deinit {
        self.fdPlayer = nil
        self.vBird.fdPlayer = nil
        self.vMulti.vFisrt.fdPlayer = nil
        self.vMulti.vSecond.fdPlayer = nil
        self.vMulti.vThird.fdPlayer = nil
    }
    
    // TODO: 테이블 높이 동적으로 변경
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        
        self.isLive = true
        setupPlayer(vPlayerContain, rect: vPlayerContain.bounds)
        self.fdPlayer.isPosition = false
        
        self.startStreamIndicator()
        if let id = event?["id"] as? String {
            requestContents(id, next: nil)
        }
        let formatter = DateFormatter()
        if let timeZone = event?["venue_timezone_name"] as? String {
            formatter.timeZone = TimeZone(identifier: timeZone)
            if let aString = event?["scheduled_at"] as? String {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                let aDate: Date = formatter.date(from: aString)!
                formatter.dateFormat = "MMM d"
                lbTitleDescript.text = "NBA | " + formatter.string(from: aDate)
            }
        }
        
        #if false
        // for test
        //self.progress.atStartTime = 1618971392
        //self.progress.atStartTime = 1618982192 //64800
//        self.progress.atStartTime = 1619053900 // 360  1619047532
        streamOpen("rtsp://192.168.0.102:554/live.4ds?type=live")
        //streamOpen("rtsp://10.82.5.97/mbc_test03.4ds?type=vod&quality=fhd&target=30")
//        _ = vBird.fdPlayer.streamOpen("rtsp://10.82.5.100/live.4ds?type=live&group=2&target=5", isTCP: true, isHWAccel: true);
//        vLandScapeMuti.vFisrt.openStream("rtsp://10.82.5.98/mbc_test03.4ds?type=vod&quality=fhd&target=10")
//        vLandScapeMuti.vSecond.openStream("rtsp://10.82.5.98/mbc_test03.4ds?type=vod&quality=fhd&target=10")
//        vLandScapeMuti.vThird.openStream("rtsp://10.82.5.98/mbc_test03.4ds?type=vod&quality=fhd&target=10")
//        vMulti.vFourth.openStream("rtsp://10.82.5.98/mbc_test03.4ds?type=vod&quality=fhd&target=10")
        #endif
        
        onRotate(false)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            vLandScapeMuti.vSecond.configViewPort()
        } else {
            
        }
    }
                        
    override func setupLayout() {
        self.navigationController?.navigationBar.isHidden = true
        self.vTable.backgroundColor = UIColor.white
        
        vMulti.vSecond.isCrop = true
        vPlayerContain.delegate = self
        vBird.delegate = self

        self.progress.isLive = true
        self.progress.setTimeShiftMode(false)
        self.progress.lbProgress.isHidden = true        
        if let font = UIFont(name: "FANtasticFOX-BoldCondensed", size: 15) {
            segment.setTitleFont(font)
        }
        
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureHandler(_:)))
        vSliding.addGestureRecognizer(panRecognizer)
        
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(multiTaptHandler(_:)))
        tapRecognizer.cancelsTouchesInView = false
        vLandScapeMuti.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func multiTaptHandler(_ recognizer: UITapGestureRecognizer) {
        self.isControls = !self.isControls
        enableControls(self.isControls)
        swipePad.isHidden = true
        timePad.isHidden = true
    }
        
    @objc func panGestureHandler(_ recognizer: UIPanGestureRecognizer) {
        let location = recognizer.translation(in: self.view)
        switch recognizer.state {
        case .began:
            break
        case .changed:
            break
        case .ended:
            let absolute = abs(location.y)
            let sign = Int(location.y).signum()
            if absolute > 60 {
                sign > 0 ? dwonSliding() : upSliding()
            } else {
                sign < 0 ? dwonSliding() : upSliding()
            }
            break
        default:
            break
        }
    }
    
    private func upSliding() {
        upSlideConstraint.isActive = true
        downSlideConstraint.isActive = false
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    private func dwonSliding() {
        upSlideConstraint.isActive = false
        downSlideConstraint.isActive = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc override func enterForeground(noti: NSNotification) {
        //LOGD(to: "\(#function)")
        fdPlayer.playToNow()
        vBird.playToNow()
        vPipContain.playToNow()
        vMulti.playToNow()
    }
    
    @objc override func enterBackground(noti: NSNotification) {
        //LOGD(to: "\(#function)")
        fdPlayer.pause()
        vBird.pause()
        vPipContain.pause()
        vMulti.pause()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LandScapePlayerIdentifier"{
            let vc = segue.destination as! LandscapeViewController
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            if sender != nil {
                vc.content = sender as? [String : AnyObject]
            }
        }
    }
                    
    // MARK: - Event & Stream Info
    private func requestContents(_ id: String?, next: String?) {
        let message = Content.init()
        message.event_id = id
        message.next = next
        _ = message.makeRequest()
        LOGD(to: "Request event: \(String(describing: message.cmd)) ",
            "url: \(String(describing: message.request?.description))")
        guard let request = message.request else {
            LOGD(to: "Fail to make event request")
            return
        }
        
        let response = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            DispatchQueue.main.async {
                self.btReload.stopRotate()
            }
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.contains("json"),
                let aData = data,
                let ret: [String : Any] = aData.dictionaryFromJson(),
                error == nil
                else { return }
            if let contents: [[String : AnyObject]] = ret["contents"] as? [[String : AnyObject]] {
                self.contents = contents
                if self.isRequest == false {
                    self.isRequest = true
                    if let content = contents.first {
                        self.contentList = contents
                        self.contentList?.remove(at: 0)
                        if let obj = self.contentList?.last {
                            if obj["next"] != nil {
                                self.contentList?.removeLast()
                            }
                        }
                        if self.isOpened != true {
                            DispatchQueue.main.async {
                                self.configPlayInfo(content)
                                self.configLandScapeInfo(content)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.showMessageAlert(message: MSG_LOAD_CONTENT_ERROR, title: "Error")
                            self.stopStreamIndicator()
                            if let button = self.btReStart {
                                button.isHidden = false
                                button.superview?.bringSubviewToFront(button)
                            }
                        }
                    }
                } else {
                    self.contentList?.append(contentsOf: contents)
                    let obj: [String : AnyObject] = (self.contentList?.last)!
                    if obj["next"] != nil {
                        self.contentList?.removeLast()
                    }
                }
                DispatchQueue.main.async {
                    self.vTable.reloadData()
                }
            } else {
                LOGD(to: "Request event not exist field.")
            }
            if let page: [String : AnyObject] = ret["page"] as? [String : AnyObject] {
                if page["next"] != nil && !(page["next"] is NSNull) {
                    self.contentList?.append(page)
                }
            }
        }
        FDConnection.dataTaskWithRequest(request, block: response)
    }
    
    private func configPlayInfo(_ content: [String : AnyObject]) {
        if let metaInfo = content["event_meta_info"] {
            lbLeftName.text = metaInfo["home_team_name"] as? String
            lbRightName.text = metaInfo["away_team_name"] as? String
            lbLeftScore.text = "\(metaInfo["home_team_score"] as? Int ?? 0)"
            lbRightScore.text = "\(metaInfo["away_team_score"] as? Int ?? 0)"
            lbQuater.text = metaInfo["quater"] as? String
            updateThumnail(metaInfo as! Dictionary<String, Any>)
        }
        
        if let value = content["started_at"] as? NSNumber {
            print(" start at: \(value)")
            let atStart: Int64 = Int64(truncating: value)
            print(" int value: \(atStart)")
            self.progress.atStartTime = atStart//100
        }
        
        if FDUtill.checkNetwork() != true {
            vPlayerContain.showToastMessage("There was a problem with the network.")
            return
        }
        // TODO: 조건 검토할것
        if isOpened != true {
            // MARK: Stream open()
            if let baseUrl = content["stream_url"] as? String {
                /* Main */
                var ret = getStreamInfoWithName("MAINVIEW", baseUrl: baseUrl, content: content)
                LOGD(to: "Main stream url: \(String(describing: ret.streamUrl))")
                streamOpen(ret.streamUrl)
                
                if btBird.isSelected == true {
                    /* Bird */
                    ret = getStreamInfoWithName("BIRDVIEW", baseUrl: baseUrl, content: content)
                    LOGD(to: "Bird stream url: \(String(describing: ret.streamUrl))")
                    let strUrl = vBird.openStream(baseUrl, channel: ret.channel)
                    vPipContain.streamUrl = strUrl
                    
                    ret = getStreamInfoWithName("MULTIVIEW", baseUrl: baseUrl, content: content)
                    vMulti.setupStream(baseUrl, channel: ret.channel)
                    vLandScapeMuti.setupStream(baseUrl, channel: ret.channel)
                } else {
                    /* Multi */
                    ret = getStreamInfoWithName("MULTIVIEW", baseUrl: baseUrl, content: content)
                    LOGD(to: "Multi stream url: \(String(describing: ret.streamUrl))")
                    vMulti.openStream(baseUrl, channel: ret.channel)
                    vLandScapeMuti.openStream(baseUrl, channel: ret.channel)
                    
                    ret = getStreamInfoWithName("BIRDVIEW", baseUrl: baseUrl, content: content)
                    let strUrl = vBird.setupStream(baseUrl, channel: ret.channel)
                    vPipContain.streamUrl = strUrl                    
                }
            }
        }
    }
    
    private func updateThumnail(_ info: Dictionary<String, Any>) {
        let leftUrl = info["home_team_logo_url"] as? String ?? ""
        let rightUrl = info["away_team_logo_url"] as? String ?? ""
        FDConnection.requestImageWithTarget(ivLeft, url: leftUrl)
        FDConnection.requestImageWithTarget(ivRight, url: rightUrl)
    }
    
    private func getStreamInfoWithName(_ name: String!, baseUrl: String!, content: [String : AnyObject])
    -> (streamUrl: String, channel: Channel) {
        
        var streamUrl: String = baseUrl
        let channel: Channel = Channel.init()
        if let groupInfo = content["group_info"] as? [String : AnyObject],
           let groups: [[String : AnyObject]] = groupInfo["groups"] as? [[String : AnyObject]] {
            
            let predicate = NSPredicate(format: "name like %@", name)
            let array: Array = groups.filter{ predicate.evaluate(with: $0)}
            if let group: [String : AnyObject] = array.first {
                if let groupId = group["id"] as? String {
                    streamUrl = "\(streamUrl)&group=\(groupId)&target=1"
                }
                if name == "MAINVIEW" {
                    if let count = group["channel_count"] as? NSNumber {
                        channelCount = Int(truncating: count)
                        vBird.totalCount = Int(truncating: count)
                    }
                    if let channel = group["default_channel_id"] as? String {
                        vBird.leftIndex = Int(channel)!
                    }
                }
                channel.parsing(group: group)
            }
        }
        
        return (streamUrl, channel)
    }
    
    private func configLandScapeInfo(_ event: [String : AnyObject]) {
        lbTitle.text = event["title"] as? String
        let descript = event["description"] as? String
        let scheduel = event["scheduled_at"] as? String
        var vs = ""
        if let metaInfo = event["event_meta_info"] {
            var home = ""
            var away = ""
            if let aString = metaInfo["home_team_name"] as? String {
                home = aString
            }
            if let aString = metaInfo["away_team_name"] as? String {
                away = aString
            }
            vs = "\(home) vs \(away)"
        }
        lbDescript.text = "\(descript ?? "") | \(vs) | \(scheduel ?? "")"
    }
    
    override func updateChannel(_ channel: Int) {
        print("\(#function)>>>>>> channel: \(channel)")
        vBird.updateCameraPosition(channel)
    }

    // MARK: - Button Action
    @IBAction func backButtonTouchUpInside(_ sender: Any?) {
        if self.fdPlayer != nil {
            self.fdPlayer.streamClose()
            //self.fdPlayer = nil
        }        
        vBird.close()
        vMulti.close()
        self.dismissController()
    }
    
    @IBAction func rotateButtonTouchUpInside(_ sender: Any?) {
        if let button: UIButton = sender as? UIButton {
            button.isSelected = !button.isSelected
        }
        
        //btAutoSwitch.setOn(on: false, animated: true)
        //self.fdPlayer.setDisplay(CGPoint.zero, scale: 1.0)
        //onAutoTrakingMode(false)
        //vPlayerContain.zoomScale = 1.0
        
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .unknown, .portrait, .portraitUpsideDown, .faceUp, .faceDown:
            AppUtility.lockOrientation(.landscapeRight)
            let value = UIDeviceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            onRotate(true)            
            if let guideView = self.vLandScapeGuide {
                self.showGuideView(guideView, key: "USER_GUIDE_LANDSCAPE")
            }
            break
        case .landscapeLeft, .landscapeRight:
            AppUtility.lockOrientation(.portrait)
            let value = UIDeviceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            onRotate(false)
            break
        default:
            break
        }
    }
        
    func onRotate(_ isLandscapeMode: Bool) {
        self.isLandscape = isLandscapeMode
        if isLandscapeMode == true {
            vTop.isHidden = true
            vMidContain.isHidden = true
            vSliding.isHidden = true
            vInterval.isHidden = true
            playerHeightConstraint.isActive = true
            playerHeightConstraint.constant = self.view.bounds.height
            vTopbg.isHidden = false
            vBottombg.isHidden = false
            swipePad.isHidden = false
            timePad.isHidden = false
            //btPipShow.isHidden = false
            segment.isHidden = false
            
            vPipContain.isHidden = false
            vPipContain.playToNow()
        } else {
            vTop.isHidden = false
            vMidContain.isHidden = false
            vSliding.isHidden = false
            vInterval.isHidden = false
            playerHeightConstraint.isActive = false
            vTopbg.isHidden = true
            vBottombg.isHidden = true
            swipePad.isHidden = true
            timePad.isHidden = true
            btPipShow.isHidden = true
            segment.isHidden = true
            
            if isRequest == true {
                switchLivePlayView(0)
            }
            vPipContain.isHidden = true
            vPipContain.pause()
            segment.selectedSegmentIndex = 0
        }
    }
    
    @IBAction func pipButtonTouchUpInside(_ sender: Any?) {
        if let button = sender as? UIButton {
            if button == btPipClose {
                vPipContain.fadeOut()
                btPipShow.isHidden = false
            } else if button == btPipShow {
                vPipContain.fadeIn()
                btPipShow.isHidden = true
            }
        }
    }
    
    @IBAction func switchButtonTouchUpInside(_ sender: Any?) {
        if let button = sender as? UIButton {
            if button == btBird {
                if button.isSelected == false {
                    button.isSelected = true
                    btMulti.isSelected = false
                    vMulti.isHidden = true
                    vBird.isHidden = false
                    vBird.playToNow()
                    vMulti.pause()
                }
            } else if button == btMulti {
                if button.isSelected == false {
                    button.isSelected = true
                    btBird.isSelected = false
                    vMulti.isHidden = false
                    vBird.isHidden = true
                    vMulti.playToNow()
                    vBird.pause()
                }
            }
        }
    }
    
    /* Birdview, multiview streaming */
    @IBAction func reopenButtonTouchUpInside(_ sender: Any?) {
        if  btBird.isSelected == true {
            vBird.close()
            _ = vBird.openStream(nil, channel: nil)
        } else if btMulti.isSelected == true {
            vMulti.close()
            vMulti.openStream(nil, channel: nil)
        }
    }
    
    @IBAction func reloadButtonTouchUpInside(_ sender: Any?) {
        if let button = sender as? UIButton {
            button.startRotate()
        }
        if let id = event?["id"] as? String {
            isRequest = false
            self.contentList?.removeAll()
            requestContents(id, next: nil)
        }
    }
    
    @IBAction func segmentValueChanged(_ sender: Any) {
        let segment = sender as! UISegmentedControl
        LOGD(to:"\(segment.selectedSegmentIndex)")
        switchLivePlayView(segment.selectedSegmentIndex)
    }
    
    private func switchLivePlayView(_ index: Int) {
        switch index {
        case 0:
            fdPlayer.playToNow()
            vBird.playToNow()
            vLandScapeMuti.pause()
            onMutilMode(false)
            break
        case 1:
            fdPlayer.pause()
            vBird.pause()
            vLandScapeMuti.playToNow()
            onMutilMode(true)
            break
        default:
            break
        }
    }
    
    internal func onMutilMode(_ isMulti: Bool) {
        swipePad.base.isHidden = isMulti
        swipePad.stick.isHidden = isMulti
        timePad.base.isHidden = isMulti
        timePad.stick.isHidden = isMulti
        
        vPlayerContain.isHidden = isMulti
        vPipContain.isHidden = isMulti
        vLandScapeMuti.isHidden = !isMulti
        
        progress.slider.isHidden = isMulti
        progress.lbDuration?.isHidden = isMulti
        progress.btPause.isHidden = isMulti
        //progress.btNow?.isHidden = isMulti
        progress.ivLive?.isHidden = isMulti
    }
    
    internal func onAutoTrakingMode(_ isMode: Bool) {
        self.isAutoMode = isMode
        swipePad.base.isHidden = isMode
        swipePad.stick.isHidden = isMode
        timePad.base.isHidden = isMode
        timePad.stick.isHidden = isMode
        vBird.onAutoTrakingMode(isMode)
        progress.slider.isHidden = isMode
        if isMode == false && self.fdPlayer.isDisplaySize == true {
            self.fdPlayer.isDisplaySize = false
        }
    }
}

// MARK: - UITable Delegate, DataSource
extension LiveViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        if let obj: [String : AnyObject] = contentList?[indexPath.row] {
            if obj["next"] == nil {
                if let cell: HighlightCell = tableView.dequeueReusableCell(withIdentifier: "HighlightCellIdentifier",
                                                                            for: indexPath) as? HighlightCell {
                    cell.configCell(obj)
                    return cell
                }
            } else {
                if let cell: NextPageCell = tableView.dequeueReusableCell(withIdentifier: "NextPageIndentifier",
                                                                          for: indexPath) as? NextPageCell {
                    if (tableView.indexPathsForVisibleRows?.contains(indexPath))! {
                        if let next: String = obj["next"] as? String  {
                            cell.startIndicator()
                            requestContents(nil, next: next)
                        }
                    } else {
                        cell.stopIndicator()
                    }
                    return cell
                }
            }
        }
        
        return NextPageCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LOGD(to: "\(#function) index: \(indexPath.row)");
        if let obj: [String : AnyObject] = contentList?[indexPath.row] {
            performSegue(withIdentifier: "LandScapePlayerIdentifier", sender: obj)
            fdPlayer.pause()
            vMulti.pause()
            vBird.pause()
            
            btAutoSwitch.setOn(on: false, animated: true)
            self.fdPlayer.setDisplay(CGPoint.zero, scale: 1.0)
            onAutoTrakingMode(false)
            vPlayerContain.zoomScale = 1.0
        }
    }
}

// MARK: - Delegate
extension LiveViewController: MultiViewDelegate, SwieeftSwitchButtonDelegate, BirdViewDelegate {
    func selectCameraPosition(index: Int) {
        print("\(#function)>>>>>> index: \(index)")
        if self.currentChannel != index {
            self.fdPlayer.setTargetChannel(index)
        }
    }
    @objc func isOnValueChange(isOn: Bool) {
        self.isAutoMode = isOn
        var message = ""
        if isOn != true {
            self.fdPlayer.setDisplay(CGPoint.zero, scale: 1.0)
            self.onAutoTrakingMode(false)
            message = "Auto Interactive Streaming OFF"
        } else {
            self.onAutoTrakingMode(true)
            message = "Auto Interactive Streaming ON"
        }        
        self.vPlayerContain.showToastMessage(message)
    }
    
    @objc func selectedMultiView(_ view: MultiItemView, index: Int, streamUrl: String?) {
        LOGD(to: "\(#function) index: \(index) stream: \(String(describing: streamUrl))")
        /*
        if let url = streamUrl {
            self.closeComplete = { [weak self] in
                DispatchQueue.main.async {
                    self?.startStreamIndicator()
                    self?.streamOpen(url)
                }
            }
            self.fdPlayer.streamClose()
        }
        */
    }
    
    // MARK: Position
    func getRenderWillUpdate(_ player: FDLivePlayer, channel: Int32) {
        if player.isPosition {
            // TODO: 락처리
            if self.positionChannels.contains(Int(channel)) {
                //print("\(#function) channel: \(channel)")
                self.arrangeOffset(channel)
                self.positionChannels.remove((Int(channel)))
            }
        }
    }
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.isAutoMode == true {
            scrollView.zoomScale = 1.0
            return
        }
        if scrollView.zoomScale <= 1.0 {
            vPosition.isHidden = true
            fdPlayer.isPosition = false
            return
        }
        self.fdPlayer.isPosition = true
        if self.fdPlayer.isPosition == true && self.isResize == true {
            vPosition.isHidden = false
            
            let offset = scrollView.contentOffset
            
            /* cetner */
            var retX = (offset.x + scrollView.frame.width / 2)
            var retY = (offset.y + scrollView.frame.height / 2)
            /* ratio */
            let ratioX = CGFloat(self.wResolution) / scrollView.contentSize.width
            let ratioY = CGFloat(self.hResolution) / scrollView.contentSize.height
            print("thyung>>>>>> 00 ratio x:\(ratioX) y: \(ratioY) zoom: \(scrollView.zoomScale)");
            
            /* marker */
            let lenght: CGFloat = CGFloat(fdPlayer.getPositionLenght())
            //let makerRatio = CGFloat(self.hResolution) / scrollView.frame.height
            let makerLength = lenght / ratioY
            print("maker lenght: \(lenght) makerLenght: \(makerLength)");
            
            // 1920, 1080
            retX = retX * ratioX
            retY = retY * ratioX// - lenght
            let ret = fdPlayer.setVecPositionAxis(self.currentChannel,
                                                   scale: Float(scrollView.zoomScale),
                                                   posX: Int32(retX),
                                                   posY: Int32(retY))
            print("current: \(self.currentChannel), postion:\(self.currentChannel)")
            print("ret: \(ret) = setVecPositionAxis(channel: \(self.currentChannel) "
                  + "scale: \(scrollView.zoomScale) posX: \(retX) posY: \(retY)")
            print("Maker lenght: \(lenght) origin: \(lenght * ratioY)")
            vPosition.setPostionLengthWithScale(scrollView.zoomScale, length: makerLength)
        }
    }
}

