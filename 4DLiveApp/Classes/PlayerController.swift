/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

enum PadTouch: Int {
    case end
    case begin
}

enum Pads: Int {
    case swipe
    case time
}

class PlayerController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var vPlayerContain: PlayerContainView!
    @IBOutlet weak var progress: ProgressView!
    @IBOutlet weak var swipePad: ControlPad!
    @IBOutlet weak var timePad: ControlPad!
    @IBOutlet weak var btReStart: UIButton?
    @IBOutlet weak var streamIndicator: UIActivityIndicatorView?    
    @IBOutlet weak var vTopbg: UIView!
    @IBOutlet weak var vBottombg: UIView!
    @IBOutlet weak var lbEnded: UILabel?

    internal var didRequestCompletion: ( [String: String]? ) -> () = { items in }
    internal var openComplete: ( (FDLivePlayer?) -> Void)?
    internal var pauseComplete: (() -> Void)?
    internal var playComplete: (() -> Void)?
    internal var closeComplete: (() -> Void)?
    internal var errorHandler: ( (FDLivePlayer?, Int32) -> Void)?
    internal var swipeHandler: ( (Bool, String) -> Void )?
    
    internal var fdPlayer: FDLivePlayer!
    internal var streamUrl: String?
    internal var isLive = false
    internal var isOpened = false
    private var isPause = false
    private var isLivePause = false
    internal var isPlaying = false
    internal var isLandscape = false
    internal var isTimeShift = false
    internal var isControls = true
    
    private var distanceX: CGFloat = 0.0
    private var padState: Int = 0
    private var thread: Thread?
    private var timer: Timer?
    private var livePauseTime: Int64 = 0
    internal var isAutoMode = false
    
    /* Postion swipe */
    internal var wResolution: Int32 = 1920
    internal var hResolution: Int32 = 1080
    internal var currentChannel: Int32 = 0
    internal var targetChannel: Int32 = 0
    internal var positionChannels: Set<Int> = []
    internal var isResize: Bool = true
    var screenScale: CGFloat = 1.0
    var event: [String : AnyObject]?
    
    open func updateChannel(_ channle: Int) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenScale = UIScreen.main.scale        
        lbEnded?.isHidden = true
        progress.delegate = self
        if let button = btReStart {
            button.isHidden = true
        }
        enableControls(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    internal func setupPlayer(_ target: UIView, rect: CGRect) {
        if fdPlayer == nil {
            /* FDLPlayer */
            fdPlayer = FDLivePlayer.init(delegate: self)
            fdPlayer.playerView.frame = rect
            fdPlayer.playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            target.insertSubview(fdPlayer.playerView, at: 0)
            
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(taptHandler(_:)))
            tapRecognizer.cancelsTouchesInView = false
            fdPlayer.playerView.addGestureRecognizer(tapRecognizer)
            
            let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(panRecognized(_: )))
            fdPlayer.playerView.addGestureRecognizer(panRecognizer)
        }
        
        fdPlayer.playerView.frame = rect
        fdPlayer.playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
        
    internal func streamOpen(_ url: String?) {
        if url == nil { return }
        
        DispatchQueue.main.async {
            self.lbEnded?.isHidden = true
        }
        //showIndicator()
        self.streamUrl = url
        var ret = -1
        //let strUrl = url /* rtsp address */

        let aUrl = NSURL(string: url!)
        let strIP = "http:\(aUrl?.host ?? "")" /* live server address */
        let port = 7070 /* Server port number */
        
        ret = Int(fdPlayer.streamOpen(url!, isTCP: true, isHWAccel: true));//MARK: Open()
        LOGD(to: "rtsp address: \(String(describing: url))")
        if ret != 0 { closeIndicator() }
        ret = Int(fdPlayer.restFulOpen(strIP, port: port))
        if ret != 0 { closeIndicator() }
    }
    
    /* Request rtsp streaming url. */
    internal func requestStreamUrl(_ url: String, id: String?, type: String?) {
        let message = LSRequest.init(withAddress: url)
        message.id = id
        message.type = type
        _ = message.makeRequest()
        let task = URLSession.shared.dataTask(with: message.request!) { (data, response, error) in
            if let error = error {
                LOGD(to: "Error took place \(error)")
                return
            }
            if data != nil {
                let ret = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : String]
                self.didRequestCompletion(ret)
            } else {
                self.didRequestCompletion(nil)
            }
        }
        task.resume()
    }
    
    internal func requestContentStatus(_ content: [String : AnyObject],
                                       completionHandler: @escaping ([String : AnyObject]?) -> Void) {
        
        guard let id = content["id"] as? String else { return }
        let message = Content.init()
        message.isStatus = true
        message.id = id
        _ = message.makeRequest()
        LOGD(to: "Request event: \(String(describing: message.cmd)) ",
            "url: \(String(describing: message.request?.description))")
        guard let request = message.request else {
            LOGD(to: "Fail to make event request")
            return
        }
        
        let response = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.contains("json"),
                let aData = data,
                let ret: [String : Any] = aData.dictionaryFromJson(),
                error == nil
                else { return }
            
            completionHandler(ret as [String : AnyObject])
            self.closeIndicator()
        }
        FDConnection.dataTaskWithRequest(request, block: response)
        
    }
        
    @objc func panRecognized(_ recognizer: UIPanGestureRecognizer) {
        if isLandscape == true { return }
        let distance = recognizer.translation(in: self.view)
        if recognizer.state == UIGestureRecognizer.State.changed {
            recognizer.cancelsTouchesInView = false
            let valueX = abs(distance.x)
            let valueY = abs(distance.y)
            if valueX > valueY {
                panGestureSwipe(distance: distance)
            }
        } else if recognizer.state == UIGestureRecognizer.State.ended {
            LOGD(to: "\(#function) UIGestureRecognizerStateEnded")
            distanceX = 0.0;
        }
    }
    
    func panGestureSwipe(distance: CGPoint) {
        let value = distance.x - distanceX
        let absolute = abs(value)
        if value > 0 {
            if absolute > 5 {
                let direct = "right"
                let handle: ChangeFrame = { [weak self] (isSuccess, message, error) in
                    if let handler = self?.swipeHandler {
                        handler(isSuccess, direct)
                    }
                }
                _ = isTimeShift ? fdPlayer.setChangeFrameCh("pause", direction:direct, moveFrame:1, block: handle)
                    : fdPlayer.setChangeChannel("normal", direction: "right", moveFrame:1, block: handle)
            }
        } else {
            if absolute > 5 {
                let direct = "left"
                let handle: ChangeFrame = { [weak self] (isSuccess, message, error) in
                    if let handler = self?.swipeHandler {
                        handler(isSuccess, direct)
                    }
                }
                _ = isTimeShift ? fdPlayer.setChangeFrameCh("pause", direction:"left", moveFrame:1, block: handle)
                    : fdPlayer.setChangeChannel("normal", direction: "left", moveFrame:1, block: handle)
            }
        }
        distanceX = distance.x
    }
    
    @objc func taptHandler(_ recognizer: UITapGestureRecognizer) {
        isControls = !isControls
        enableControls(isControls)
    }
    
    internal func enableControls(_ isAppear: Bool) {
        if isAppear == true {
            if isLandscape == true {
                swipePad.isEnable = true
                timePad.isEnable = true
                vTopbg.fadeIn()
                vBottombg.fadeIn()
            }
            progress.fadeIn()
            beginTimer()
        } else {
            swipePad.isEnable = false
            timePad.isEnable = false
            if isLandscape == true {
                vTopbg.fadeOut()
                vBottombg.fadeOut()
            }
            progress.fadeOut()
            invalidateTimer()
        }
    }

    internal func beginTimer() {
        invalidateTimer()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self,
                                     selector: #selector(onTick(timer:)), userInfo: nil, repeats: false)
    }
    
    internal func invalidateTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc private func onTick(timer: Timer) {
        isControls = false
        enableControls(false)
    }
    
    public func startStreamIndicator() {
        if streamIndicator != nil {
            streamIndicator!.isHidden = false
            streamIndicator!.startAnimating()
        }
    }
    
    public func stopStreamIndicator() {
        if streamIndicator != nil {
            streamIndicator!.isHidden = true
            streamIndicator!.stopAnimating()
        }
    }
    
    // MARK: - Button Action
    @IBAction func reStartButtonTouchUpInside(_ sender: Any) {        
        if streamUrl != nil {
            self.closeComplete = { [weak self] in
                DispatchQueue.main.async {
                    self?.startStreamIndicator()
                    self?.streamOpen(self?.streamUrl)
                    self?.progress.setTimeShiftMode(false)
                    let button = sender as! UIButton
                    button.isHidden = true
                }
            }
            fdPlayer.streamClose()
        }
    }
    
    @IBAction func pauseButtonTouchUpInside(_ sender: Any?) {
        var button: UIButton
        if sender == nil {
            button = self.progress.btPause
        }
        button = sender as! UIButton
        
        if isOpened != true {
            LOGD(to: "It is not open yet.")
            if button.isSelected == true {
                startStreamIndicator()
                streamOpen(self.streamUrl)
            } else {
                fdPlayer.streamClose()
                stopStreamIndicator()
            }
            button.isSelected = !button.isSelected
        } else {
            button.isSelected ? onPlay() : onPause()
            button.isSelected = !button.isSelected
        }
    }
    
    internal func onPlay() {
        print("\(#function)");
        if self.isPlaying != true {
            self.closeComplete = { [weak self] in
                DispatchQueue.main.async {
                    self?.startStreamIndicator()
                    self?.streamOpen(self?.streamUrl)
                    self?.progress.setTimeShiftMode(false)
                }
            }
            fdPlayer.streamClose()
        } else {
            /* play */
            fdPlayer.play()
            isPause = false
            if isLive == true {
                isTimeShift = true
            }
        }
    }
    
    internal func onPause() {
        print("\(#function)");
        /* pause */
        fdPlayer.pause()
        if isLive == true {
            isTimeShift = true
            progress.setTimeShiftMode(true)
        }
    }
    
    @IBAction func nowButttonTouchUpInside(_ sender: Any) {
        fdPlayer.playToNow()
        //timePad.isHidden = true
        isTimeShift = false
        progress.setTimeShiftMode(false)
        progress.btPause.isSelected = false
        beginTimer()
    }
        
    @objc override func enterForeground(noti: NSNotification) {
        LOGD(to: "\(#function)")
    }
    
    @objc override func enterBackground(noti: NSNotification) {
        LOGD(to: "\(#function)")
    }
    
    // MARK: ETC
    func getPlayerHeight(_ width: CGFloat) -> CGFloat {
        let height = width * CGFloat(hResolution) / CGFloat(wResolution)
        return height
    }
    
    func getPlayerWidth(_ height: CGFloat) -> CGFloat {
        let width = height * CGFloat(wResolution) / CGFloat(hResolution)
        return width
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        self.isResize  = true
        return self.fdPlayer.playerView
    }
    
    internal func arrangeOffset(_ channel: Int32) {
        //print("\(#function) channel : \(channel)")
        let nPosX = UnsafeMutablePointer<Int32>.allocate(capacity: 1)
        let nPosY = UnsafeMutablePointer<Int32>.allocate(capacity: 1)
        let retX = self.fdPlayer.getMovedPosX(channel, nPoX: nPosX);
        let retY = self.fdPlayer.getMovedPosY(channel, nPoY: nPosY);
        LOGD(to: "getMovedPosX(channel: \(channel) ) ret: \(retX) posX: \(nPosX.pointee)")
        LOGD(to: "getMovedPosY(channel: \(channel) ) ret: \(retY) posY: \(nPosY.pointee)")
        if retX != 1 || retY != 1 {
            LOGD(to: "Fail to get postion value. (X: \(retX) Y: \(retY))")
            return
        }
        DispatchQueue.main.async {
            let width = self.vPlayerContain.contentSize.width
            let height = self.vPlayerContain.contentSize.height
            let ratioX = CGFloat(nPosX.pointee) / CGFloat(self.wResolution)
            let ratioY = CGFloat(nPosY.pointee) / CGFloat(self.hResolution)
            let centerX = (self.vPlayerContain.frame.width / 2)
            let centerY = (self.vPlayerContain.frame.height / 2)
            
            let retX = width * ratioX
            let retY = height * ratioY
                        
            let valueX = retX - centerX
            let valueY = retY - centerY
            let ret = CGPoint(x: valueX, y: valueY)
            self.vPlayerContain.contentOffset = ret
            
            nPosX.deallocate()
            nPosY.deallocate()
        }
    }
}

// MARK: - ControlPad Delegate
extension PlayerController: ControlPadDelegate {
    @objc func moveBegin(_ pad: ControlPad) {
        if isOpened != true {
            LOGD(to: "It is not open yet.")
            return
        }
        
        self.padState |= PadTouch.begin.rawValue << pad.tag
        if padState != 0 && thread == nil {
            thread = Thread.init(target: self, selector: #selector(onEventThread(_:)), object: nil)
            thread?.start()
            LOGD(to: "\(#function) \(pad) thread start.")
        }
        
        if pad == self.timePad {
            if isPause != true {
                if let button = self.progress.btPause {
                    button.isSelected = false
                    self.pauseButtonTouchUpInside(button)
                    self.pauseComplete = { [weak self] in
                        self?.isPause = true
                        if self?.isLive == true {
                            //self?.progress.duration = self?.livePauseTime ?? 0
                            self?.progress.updateDuration(self?.livePauseTime ?? 0)
                        }
                    }
                    self.playComplete = { [weak self] in
                        self?.isPause = false
                        self?.isLivePause = false
                    }
                    self.progress.setTimeShiftMode(true)
                }
            }
        }
        self.enableControls(true)
        invalidateTimer()
    }
    
    @objc func moveEnd(_ pad: ControlPad) {
        if isOpened != true {
            LOGD(to: "It is not open yet.")
            return
        }
        self.padState &= ~(PadTouch.begin.rawValue << pad.tag)
        if padState == 0 && thread != nil {
            thread?.cancel()
            thread = nil
            swipePad.clearEvent()
            timePad.clearEvent()
            LOGD(to: "\(#function) \(pad) thread stop.")
        }
        beginTimer()
    }
    
    @objc func onEventThread(_ sender: Any) {
        //print("\(#function) !!! ")
        autoreleasepool {
            repeat {
                if  self.thread == nil || self.thread!.isCancelled {
                    break
                }
                                
                let interval = 0.04
                let frame = 1
                let swipe = self.swipePad.getEvent()
                let time = self.timePad.getEvent()
                let fHandle: ChangeFrame = { [weak self] (isSuccess, message, error) in
                    if let handler = self?.swipeHandler { handler(isSuccess, "right") }
                }
                let cHandle: ChangeChannel = { [weak self] (isSuccess, message, error) in
                    if let handler = self?.swipeHandler { handler(isSuccess, "right") }
                }
                if swipe != nil && time == nil { /* swipe */
                    if swipe ==  DirectionToPad.right.rawValue {
                        _ = (isTimeShift == true)
                            ? fdPlayer.setChangeFrameCh("pause", direction:"right", moveFrame: frame, block: fHandle)
                            : fdPlayer.setChangeChannel("normal", direction: "right", moveFrame:frame, block: cHandle)
                    }
                    if swipe ==  DirectionToPad.left.rawValue {
                        _ = (isTimeShift == true)
                            ? fdPlayer.setChangeFrameCh("pause", direction:"left", moveFrame: frame, block: fHandle)
                            : fdPlayer.setChangeChannel("normal", direction: "left", moveFrame:frame, block: cHandle)
                    }
                }
                //left backward  right forward
                if time != nil && swipe == nil { /* timeshift */
                    if time ==  DirectionToPad.left.rawValue {
                        fdPlayer.setChangeFrameCh("rewind", direction:"stop", moveFrame: 1, block: nil) /* left */
                        print("thyung>>>>>> rewind");
                    }
                    if time ==  DirectionToPad.right.rawValue {
                        fdPlayer.setChangeFrameCh("forward", direction:"stop", moveFrame: 1, block: nil) /* right */
                        print("thyung>>>>>> forward");
                    }
                }
                if time != nil && swipe != nil { /* swipe & time shift */
                    let rHandle: ChangeFrame = { [weak self] (isSuccess, message, error) in
                        if let handler = self?.swipeHandler { handler(isSuccess, "right") }
                    }
                    let lHandle: ChangeFrame = { [weak self] (isSuccess, message, error) in
                        if let handler = self?.swipeHandler { handler(isSuccess, "left") }
                    }
                    /* left & left */
                    if time ==  DirectionToPad.left.rawValue
                        && swipe! & DirectionToPad.left.rawValue ==  DirectionToPad.left.rawValue {
                        fdPlayer.setChangeFrameCh("rewind", direction:"left", moveFrame:frame, block: lHandle)
                    }
                    /* right & left*/
                    if time ==  DirectionToPad.left.rawValue
                        && swipe! & DirectionToPad.right.rawValue ==  DirectionToPad.right.rawValue {
                        fdPlayer.setChangeFrameCh("rewind", direction:"right", moveFrame:frame, block: rHandle)
                    }
                    /* left & right*/
                    if time ==  DirectionToPad.right.rawValue && swipe ==  DirectionToPad.left.rawValue {
                        fdPlayer.setChangeFrameCh("forward", direction:"left", moveFrame:frame, block: lHandle)
                    }
                    /* right & right */
                    if time ==  DirectionToPad.right.rawValue && swipe ==  DirectionToPad.right.rawValue {
                        fdPlayer.setChangeFrameCh("forward", direction:"right", moveFrame:frame, block: rHandle)
                    }
                }
                Thread.sleep(forTimeInterval: interval)
            } while true
        }
        Thread.exit()
    }
}

// MARK: - Progress Delegate
extension PlayerController: ProgressViewDelegate {
    @objc func sliderMoveBegin(_ slider: UISlider) {
        self.invalidateTimer()
        if let button = self.progress.btPause {
            if button.isSelected == false {
                self.pauseButtonTouchUpInside(button)
                self.pauseComplete = { [weak self] in
                    self?.isPause = true
                    if self?.isLive == true {
                        self?.progress.updateDuration(self!.livePauseTime)
                        self?.isLivePause = true
                    }
                }
            }
        }
    }
    
    @objc func sliderMoveEnd(_ slider: UISlider, time: Int, base: Float) {
        LOGD(to: "\(slider) time: \(time)")
        print("\(slider) time: \(time)")
        if fdPlayer != nil {
            fdPlayer.seek(time)
        }
        //self.beginTimer()
    }
    
    @objc func progressFinish(_ slider: UISlider) {
        if isLive == true {
            // TODO: 조건 수정할것
//            fdPlayer.playToNow()
//            isTimeShift = false
//            progress.setTimeShiftMode(false)
//            progress.btPause.isSelected = false
//            beginTimer()
        }
    }
}

// MARK: - FDLivePlayer Delegate
extension PlayerController:  FDPlayerDelegate {
    //*
    func getCurrentPlayInfo(_ player:FDLivePlayer,
                            channel: Int32,
                            frame: Int32,
                            frameCycle: Int32,
                            time: Int32,
                            utc: String,
                            type: Int32,
                            timeshiftSec: Int32) {
        if player == self.fdPlayer {
            //LOGD(to: "channel: \(channel), frame: \(frame) cycle: \(frameCycle) time: \(time) tuc: \(utc)")
            //print("current info channel: \(channel), frame: \(frame) cycle: \(frameCycle) time: \(time) tuc: \(utc)")
            if time > 0 {
                if self.isLive == true {
                    if self.isTimeShift == true {
                        DispatchQueue.main.async { [weak self] in
                            self?.progress.updatePlayTime(Int64(time))
                        }
                    }
                    self.livePauseTime  = Int64(time)
                } else {
                    DispatchQueue.main.async { [weak self] in
                        self?.progress.updatePlayTime(Int64(time))
                    }
                }
            }
            /* position swipe */
            if self.currentChannel != channel {
                self.currentChannel = channel
                self.updateChannel(Int(channel))
                print("position channel: \(channel)");
                if isPause == true {
                    self.arrangeOffset(channel)
                } else {
                    if player.isPosition {
                        self.positionChannels.insert(Int(self.currentChannel))
                    }
                }
            }
        }
    }
    
    func getVideoStreamInfo(_ player:FDLivePlayer,
                            width: Int32,
                            height: Int32,
                            duration: Int32,
                            videoCodec: String,
                            audioCodec: String) {
        LOGD(to: "\(#function) width: \(width) height: \(height) duration: \(duration).")
        if player == self.fdPlayer  {
            self.wResolution = width
            self.hResolution = height
            if self.isLive == false {
                self.progress.duration = Int64(duration)
            }
        }
    }
    
    func getStart(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
        if player == self.fdPlayer {
            if code == 0 {
                isOpened = true
            }
            DispatchQueue.main.async {[weak self] in
                self?.beginTimer()
                //self?.progress.btPause.isSelected = false
                self?.closeIndicator()
                self?.stopStreamIndicator()
                //if let button = self?.btReStart { button.isHidden = true }
            }
        }
        if let handler = self.openComplete {
            handler(player)
        }
    }
    
    func getStop(_ player:FDLivePlayer, code: Int32) {
        LOGD(to: "\(#function) code: \(code).")
        self.isPlaying = false
        if code == 0 {
            if let handler = self.closeComplete {
                handler()
            }
        }
    }
    
    func getPlay(_ player:FDLivePlayer) {
        if player == self.fdPlayer {
            isPlaying = true
            DispatchQueue.main.async {[weak self] in
                if let button = self?.btReStart { button.isHidden = true }
                self?.progress.btPause.isSelected = false
                if let button = self?.btReStart { button.isHidden = true }
            }
            if let handler = self.playComplete {
                handler()
            }
        }
    }
    
    func getPause(_ player:FDLivePlayer) {
        LOGD(to: "\(#function)")
        self.isPause = true
        if player == self.fdPlayer {
            if let handler = self.pauseComplete {
                handler()
            }
        }
    }
    
    func getPlayDone(_ player:FDLivePlayer) {
        LOGD(to: "\(#function)")
        if player == self.fdPlayer {
            isPlaying = false
        }
        DispatchQueue.main.async {[weak self] in
            if player.isLoop != true {
                if let button = self?.btReStart {
                    button.isHidden = false
                    button.superview?.bringSubviewToFront(button)
                }
            }
            self?.progress.btPause.isSelected = true
        }
    }
    
    func getNalSEI(_ player: FDLivePlayer, uuid: String, metadata: NSObjectProtocol) {
        if self.isAutoMode != true { return }
        
        if let info = metadata as? [String : AnyObject],
           let nPosX = info["positionX"] as? Int,
           let nPosY = info["positionY"] as? Int,
           let ratio = info["ratio"] as? Int,
           let resolutionWidth = info["width"] as? Int,
           let resolutionheight = info["height"] as? Int {
            if ratio > 100  {
                DispatchQueue.main.async {
                    let width = player.playerView.frame.width
                    let height = player.playerView.frame.height
                    let scale: CGFloat = CGFloat(ratio) / 100.0
                    let ratioX = CGFloat(nPosX) / CGFloat(resolutionWidth)
                    let ratioY = CGFloat(nPosY) / CGFloat(resolutionheight)
                    let screenWidth = width * scale * self.screenScale
                    let screenHeight = height * scale * self.screenScale
                    let centerX = width / 2
                    let centerY = height / 2
                    let ptX = ratioX * screenWidth
                    let ptY = ratioY * screenHeight
                    
                    var retX = ptX - centerX * self.screenScale
                    var retY = ptY - centerY * self.screenScale                    
                    
                    if retX < 0 { retX = 0 }
                    if retY < 0 { retY = 0 }
                    if retX > screenWidth - width * self.screenScale {
                        retX = screenWidth - width * self.screenScale
                    }
                    if retY > screenHeight - height * self.screenScale {
                        retY = screenHeight - height * self.screenScale
                    }

                    let ret = CGPoint(x: retX, y: retY)
                    self.fdPlayer.setDisplay(ret, scale: scale)
                }
            }
            if let channel =  info["channel"] as? Int {
                if self.currentChannel != (channel - 1) && self.targetChannel != channel {
                    //print("\(#function)>>>>>> auto channel: \(channel)")
                    self.fdPlayer.setTargetChannel(channel - 1)
                    self.targetChannel = Int32(channel)
                }
            }
            
            LOGD(to: "\(#function)>>>>>> channel: \(String(describing: info["channel"]))"
                + "x: \(String(describing: info["positionX"]))" +
                    "y: \(String(describing: info["positionY"]))")
        }
    }
    
    func getError(_ player:FDLivePlayer, code: Int32, message: String) {
        LOGD(to: "\(#function) code: \(code) message: \(message)")
        var msg = message
        var title = ""
        switch code {
        case Int32(FD_ERR_ENG_2300.rawValue), Int32(FD_ERR_ENG_2301.rawValue):
            if let obj = self.event {
                self.requestContentStatus(obj) { [weak self] (respons: [String : AnyObject]?) in
                    if respons == nil { return }
                    if let content: [String : AnyObject] = respons!["content"] as? [String : AnyObject],
                       let status = content["event_status"] as? String,
                       status == "finished" {
                        DispatchQueue.main.async { [self] in
                            /* End streaming */
                            if let label = self?.lbEnded {
                                label.isHidden = false
                                msg = "The Broadcast has ended."
                            }
                        }
                        player.streamClose()
                    }
                }
            }
            break
        case Int32(FD_ERR_ENG_2200.rawValue),
             Int32(FD_ERR_ENG_2201.rawValue),
             Int32(FD_ERR_ENG_2203.rawValue),
             Int32(FD_ERR_ENG_2204.rawValue),
             Int32(FD_ERR_ENG_2205.rawValue):
            DispatchQueue.main.async {
                if let button = self.btReStart {
                    button.isHidden = false
                    button.superview?.bringSubviewToFront(button)
                }
            }
            break
        case Int32(FD_ERR_NET_5000.rawValue):
            if message == "BIRDVIEW" {
                //title = "Birdview"
                title = "Overview"
            }
            break
        default:
            title = "Error"
            break
        }
        if let handler = self.errorHandler {
            handler(player, code)
        }
        DispatchQueue.main.async {[weak self] in
            guard let self = self else {return}
            if msg.count > 0 || title.count > 0 {
                self.showMessageAlert(message: msg, title: title)
            }
            self.closeIndicator()
            self.stopStreamIndicator()
            
        }
    }
// */
}

