/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class HighlightCell: UITableViewCell {

    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var lbRound: UILabel!
    @IBOutlet weak var ivInteractive: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ivInteractive.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //ivPlay.isHidden = !selected
    }
    
    func configCell(_ event: Dictionary<String, Any>) {
        
        FDConnection.requestImageWithTarget(ivThumbnail, url: event["thumbnail_url"] as? String)
        lbTitle.text = event["title"] as? String ?? ""
        
        if let metaInfo = event["event_meta_info"] as? Dictionary<String, Any> {
            lbDescription.text = "NBA | " + (metaInfo["season"] as! String)
            lbRound.text = (metaInfo["quater"] as! String)
        }
        
        if let duration =  event["duration"] as? Double {
            let temp: Int = Int(duration) * 1000
            lbDuration.text = FDUtill.getDuration(Int32(temp))
        }
                
        if let groupInfo = event["group_info"] as? [String : AnyObject] {
            if let groups = groupInfo["groups"] as? [[String : AnyObject]] {
                if let group = groups.first {
                    // TODO: 필드 키 확인할것
                    if let channelIds = group["channel_ids"] as? Array<Any> {
                        if channelIds.count > 0 {
                            ivInteractive.isHidden = false
                        }
                    }
                }
            }
        }
    }
}

