/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class HighlightViewController: PlayerController {
    
    @IBOutlet weak var vTop: UIView!
    @IBOutlet weak var lbTitleDescript: UILabel!
    @IBOutlet weak var ivLeftt: UIImageView!
    @IBOutlet weak var lbLeftScore: UILabel!
    @IBOutlet weak var lbLeftName: UILabel!
    @IBOutlet weak var ivRight: UIImageView!
    @IBOutlet weak var lbRightScore: UILabel!
    @IBOutlet weak var lbRightName: UILabel!
    @IBOutlet weak var lbQuater: UILabel!
    
    
    //@IBOutlet weak var vPlayerContain: PlayerContainView!
    @IBOutlet weak var vMarker: PositionMarkerView!
    @IBOutlet weak var vMidContain: UIView!
    @IBOutlet weak var vTableContain: UIView!
    @IBOutlet weak var vTable: UITableView!
    
    @IBOutlet weak var lbTitle: UILabel! // for landscape
    @IBOutlet weak var lbDescript: UILabel!
    
    @IBOutlet weak var playerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var topViewAspectConstraint: NSLayoutConstraint!
    @IBOutlet var midContainAspectConstraint: NSLayoutConstraint!
        
    private var contentList: [[String : AnyObject]]?
    private var isFirst = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPlayer(vPlayerContain, rect: vPlayerContain.bounds)
        if let id = event?["id"] as? String {
            self.startStreamIndicator()
            requestContents(id, next: nil)
            self.fdPlayer.isLoop = true
        }
        vPlayerContain.delegate = self
        setupLayout()
        onRotate(false)
    }
    
    override func setupLayout() {
        vTable.backgroundColor = UIColor.white
    }
    
    private func configPlayInfo(_ content: [String : AnyObject]) {
        if let metaInfo = content["event_meta_info"] {
            lbLeftName.text = metaInfo["home_team_name"] as? String
            lbRightName.text = metaInfo["away_team_name"] as? String
            lbLeftScore.text = "\(metaInfo["home_team_score"] as? Int ?? 0)"
            lbRightScore.text = "\(metaInfo["away_team_score"] as? Int ?? 0)"
            lbQuater.text = metaInfo["quater"] as? String
            updateThumnail(metaInfo as! Dictionary<String, Any>)
        }
        
        if FDUtill.checkNetwork() != true {
            vPlayerContain.showToastMessage("There was a problem with the network.")
            return
        }
    }
    
    private func updateThumnail(_ info: Dictionary<String, Any>) {
        let leftUrl = info["home_team_logo_url"] as? String ?? ""
        let rightUrl = info["away_team_logo_url"] as? String ?? ""
        FDConnection.requestImageWithTarget(ivLeftt, url: leftUrl)
        FDConnection.requestImageWithTarget(ivRight, url: rightUrl)
    }
    
    private func configLandScapeInfo(_ event: [String : AnyObject]) {
        lbTitle.text = event["title"] as? String
        let descript = event["description"] as? String
        let scheduel = event["scheduled_at"] as? String
        var vs = ""
        if let metaInfo = event["event_meta_info"] {
            var home = ""
            var away = ""
            if let aString = metaInfo["home_team_name"] as? String {
                home = aString
            }
            if let aString = metaInfo["away_team_name"] as? String {
                away = aString
            }
            vs = "\(home) vs \(away)"
        }
        lbDescript.text = "\(descript ?? "") | \(vs) | \(scheduel ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Request
    static var isFirst = false
    private func requestContents(_ id: String?, next: String?) {
        let message = Content.init()
        message.event_id = id
        message.event_type = "vod"
        message.content_type = "vod"
        message.next = next
        _ = message.makeRequest()
        LOGD(to: "Request event: \(String(describing: message.cmd)) ",
            "url: \(String(describing: message.request?.description))")
        guard let request = message.request else {
            LOGD(to: "Fail to make event request")
            return
        }
        
        let response = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.contains("json"),
                let aData = data,
                let ret: [String : Any] = aData.dictionaryFromJson(),
                error == nil
                else { return }
            if let contents: [[String : AnyObject]] = ret["contents"] as? [[String : AnyObject]] {
                if self.isFirst == false {
                    self.isFirst = true
                    if let content = contents.first {
                        self.contentList = contents
                        let obj: [String : AnyObject] = (self.contentList?.last)!
                        if obj["next"] != nil {
                            self.contentList?.removeLast()
                        }
                        self.requestStream(content)
                        if self.isOpened != true {
                            DispatchQueue.main.async {
                                self.configPlayInfo(content)
                                self.configLandScapeInfo(content)
                            }
                        }
                    }
                } else {
                    self.contentList?.append(contentsOf: contents)
                    let obj: [String : AnyObject] = (self.contentList?.last)!
                    if obj["next"] != nil {
                        self.contentList?.removeLast()
                    }
                    self.contentList?.append(contentsOf: contents)
                }
                
                DispatchQueue.main.async {
                    self.reload()
                }
            } else {
                LOGD(to: "Request event not exist field.")
            }
            if let page: [String : AnyObject] = ret["page"] as? [String : AnyObject] {
                if page["next"] != nil && !(page["next"] is NSNull) {
                    self.contentList?.append(page)
                }
            }
        }
        FDConnection.dataTaskWithRequest(request, block: response)
    }
    
    func reload() {
        vTable.reloadData()
        if let cell = vTable.cellForRow(at: IndexPath(row: 0, section: 0)) {
            cell.setSelected(true, animated: false)
        }
    }
    
    private func requestStream(_ content: [String : AnyObject]) {
#if true
        if let streamUrl = content["stream_url"] as? String {
            self.streamOpen(streamUrl)
        }
#else
        if let url = content["ls_url"] as? String,
            let id = content["file_id"] as? String,
            let type = content["content_type"] as? String {
            self.didRequestCompletion = {items in
                if items != nil {
                    if let streamUrl = items?["value"] {
                        self.streamOpen(streamUrl)
                    }
                }
            }
            self.requestStreamUrl(url, id: id, type: type)
        }
#endif
    }
    
    // MARK: Position Swipe
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.zoomScale <= 1.0 {
            vMarker.isHidden = true
            fdPlayer.isPosition = false
            return
        }
        self.fdPlayer.isPosition = true
        if self.fdPlayer.isPosition == true && self.isResize == true {
            if let markerView = vMarker {
                markerView.isHidden = false
            }
            let offset = scrollView.contentOffset
            /* cetner */
            var retX = (offset.x + scrollView.frame.width / 2)
            var retY = (offset.y + scrollView.frame.height / 2)
            /* ratio */
            let ratioX = CGFloat(self.wResolution) / scrollView.contentSize.width
            let ratioY = CGFloat(self.hResolution) / scrollView.contentSize.height
            print("thyung>>>>>> 00 ratio x:\(ratioX) y: \(ratioY) zoom: \(scrollView.zoomScale)");

            /* marker */
            let lenght: CGFloat = CGFloat(fdPlayer.getPositionLenght())
            //let makerRatio = CGFloat(self.hResolution) / scrollView.frame.height
            let makerLength = lenght / ratioY
            print("maker lenght: \(lenght) makerLenght: \(makerLength)");

            // 1920, 1080
            retX = retX * ratioX
            retY = retY * ratioX// - lenght
            let ret = fdPlayer.setVecPositionAxis(self.currentChannel,
                                                   scale: Float(scrollView.zoomScale),
                                                   posX: Int32(retX),
                                                   posY: Int32(retY))
            print("current: \(self.currentChannel), postion:\(self.currentChannel)")
            print("ret: \(ret) = setVecPositionAxis(channel: \(self.currentChannel) "
                    + "scale: \(scrollView.zoomScale) posX: \(retX) posY: \(retY)")
            print("Maker lenght: \(lenght) origin: \(lenght * ratioY)")
            vMarker.setPostionLengthWithScale(scrollView.zoomScale, length: makerLength)
        }
    }
    
    func getRenderWillUpdate(_ player: FDLivePlayer, channel: Int32) {
        if player.isPosition {
            // TODO: 락처리
            if self.positionChannels.contains(Int(channel)) {
                print("\(#function) channel: \(channel)")
                self.arrangeOffset(channel)
                self.positionChannels.remove((Int(channel)))
            }
        }
    }
        
    // MARK: - Button Action
    @IBAction func rotateButtonTouchUpInside(_ sender: Any?) {
        if let button: UIButton = sender as? UIButton {
            button.isSelected = !button.isSelected
        }
        
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .unknown, .portrait, .portraitUpsideDown, .faceUp, .faceDown:
            AppUtility.lockOrientation(.landscapeRight)
            let value = UIDeviceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            onRotate(true)
            if self.vLandScapeGuide != nil {
                self.showGuideView(self.vLandScapeGuide!, key: "USER_GUIDE_LANDSCAPE")
            }
            break
        case .landscapeLeft, .landscapeRight:
            AppUtility.lockOrientation(.portrait)
            let value = UIDeviceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            onRotate(false)
            break
            
        default:
            break
        }
    }
    
    func onRotate(_ isLandscapeMode: Bool) {
        self.isLandscape = isLandscapeMode
        if isLandscapeMode == true {
            vTop.isHidden = true
            vMidContain.isHidden = true
            vTableContain.isHidden = true
            playerHeightConstraint.isActive = true
            playerHeightConstraint.constant = self.view.bounds.height
            
            topViewAspectConstraint.isActive = false
            midContainAspectConstraint.isActive = false
            
            vTopbg.isHidden = false
            vBottombg.isHidden = false
            swipePad.isHidden = false
            timePad.isHidden = false
        } else {
            vTop.isHidden = false
            vMidContain.isHidden = false
            vTableContain.isHidden = false
            playerHeightConstraint.isActive = false
            
            topViewAspectConstraint.isActive = true
            midContainAspectConstraint.isActive = true
            
            vTopbg.isHidden = true
            vBottombg.isHidden = true
            swipePad.isHidden = true
            timePad.isHidden = true
        }
    }
    
    @IBAction func repeatButtonTouchUpInside(_ sender: Any?) {
        if let button: UIButton = sender as? UIButton {
            fdPlayer.isLoop = button.isSelected
            button.isSelected = !button.isSelected
        }
    }
    
    @IBAction func backButtonTouchUpInside(_ sender: Any?) {
        if fdPlayer != nil {
            self.closeComplete = nil
            fdPlayer.streamClose()
            fdPlayer = nil
        }
        self.dismissController()
    }
}

extension HighlightViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        if let obj: [String : AnyObject] = contentList?[indexPath.row] {
            if obj["next"] == nil {
                if let cell: HighlightCell = tableView.dequeueReusableCell(withIdentifier: "HighlightCellIdentifier",
                                                                            for: indexPath) as? HighlightCell {
                    cell.configCell(obj)
                    if indexPath.row == 0 {
//                        cell.ivPlay.isHidden = false
                    }
                    return cell
                }
            } else {
                if let cell: NextPageCell = tableView.dequeueReusableCell(withIdentifier: "NextPageIndentifier",
                                                                          for: indexPath) as? NextPageCell {
                    if (tableView.indexPathsForVisibleRows?.contains(indexPath))! {
                        if let next: String = obj["next"] as? String  {
                            cell.startIndicator()
                            requestContents(nil, next: next)
                        }
                    } else {
                        cell.stopIndicator()
                    }
                    return cell
                }
            }
        }
        
        return NextPageCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LOGD(to: "\(#function) index: \(indexPath.row)");
        if indexPath.row != 0 {
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) {
                cell.setSelected(false, animated: false)
            }
        }
        
        if let content: [String : AnyObject] = contentList?[indexPath.row] {
            DispatchQueue.main.async {
                self.configLandScapeInfo(content)
            }
            #if true
            if let streamUrl = content["stream_url"] as? String {
                self.closeComplete = { [weak self] in
                    DispatchQueue.main.async {
                        self?.startStreamIndicator()
                        self?.streamOpen(streamUrl)
                    }
                }
                self.fdPlayer.streamClose()
            }
            #else
            if let url = content["ls_url"] as? String,
                let id = content["file_id"] as? String,
                let type = content["content_type"] as? String {
                self.didRequestCompletion = {items in
                    if items != nil {
                        if let streamUrl = items?["value"] {
                            DispatchQueue.main.async { [weak self] in
                                self?.startStreamIndicator()
                                self?.streamOpen(streamUrl)
                            }
                        }
                    }
                }
                self.requestStreamUrl(url, id: id, type: type)
            }
            self.fdPlayer.streamClose()
            #endif
        }
    }
}
