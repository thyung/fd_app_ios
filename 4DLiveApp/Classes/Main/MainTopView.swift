/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class MainTopView: BaseView {
    
    @IBOutlet weak var ivStatus: UIImageView!
    @IBOutlet weak var lbDescript: UILabel!
    @IBOutlet weak var ivLeft: UIImageView!
    @IBOutlet weak var ivRight: UIImageView!
    @IBOutlet weak var lbLeftName: UILabel!
    @IBOutlet weak var lbRightName: UILabel!
    @IBOutlet weak var lbLeftScore: UILabel!
    @IBOutlet weak var lbRightScore: UILabel!
    @IBOutlet weak var lbQuater: UILabel!
    
    @IBOutlet weak var btWatch: UIButton!
    @IBOutlet weak var lbSchedule: UILabel!
    
    var eventInfo: Dictionary<String, Any>?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func initVariable() {
        
    }

    func configView(_ event: Dictionary<String, Any>) {
        eventInfo = event        
        if let info: Dictionary<String, Any> = event["event_meta_info"] as? Dictionary<String, Any> {
            FDConnection.requestImageWithTarget(ivLeft, url: info["home_team_logo_url"] as? String)
            FDConnection.requestImageWithTarget(ivRight, url: info["away_team_logo_url"] as? String)
            lbLeftName.text = info["home_team_name"] as? String
            lbRightName.text = info["away_team_name"] as? String
            lbQuater.text = info["quater"] as? String
            lbLeftScore.text = "\(info["home_team_score"] as? Int ?? 0)"
            lbRightScore.text = "\(info["away_team_score"] as? Int ?? 0)"
        }
        
        var isLive = false
        if let status = event["event_status"] as? String {
            var image = "icon_list_upcoming"
            switch status {
            case "live":
                image = "icon_list_live"
                self.btWatch.isHidden = false
                self.lbSchedule.isHidden = true
                isLive = true
                break
            default:
                lbLeftScore.isHidden = true
                lbRightScore.isHidden = true
                break
            }
            ivStatus.image = UIImage(named: image)
        }

        let formatter = DateFormatter()
        if let timeZone = event["venue_timezone_name"] as? String {
            formatter.timeZone = TimeZone(identifier: timeZone)
            let temp =  formatter.timeZone.abbreviation()
            if let aString = event["scheduled_at"] as? String {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                let aDate: Date = formatter.date(from: aString)!
                if isLive ==  true {
                    formatter.dateFormat = "MMM d, HH:mm"
                    lbDescript.text = formatter.string(from: aDate) + " (" + (temp ?? "") + ")"
                } else {
                    formatter.dateFormat = "h:mm a"
                    lbSchedule.text = "Start at " + formatter.string(from: aDate) + " (" + (temp ?? "") + ")"
                    formatter.dateFormat = "MMM d"
                    lbDescript.text = formatter.string(from: aDate)
                }
            }
        } else {
            if let aString = event["scheduled_at"] as? String {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                let aDate: Date = formatter.date(from: aString)!
                if isLive ==  true {
                    formatter.dateFormat = "MMM d, HH:mm"
                    lbDescript.text = formatter.string(from: aDate)
                } else {
                    formatter.dateFormat = "h:mm a"
                    lbSchedule.text = "Start at " + formatter.string(from: aDate)
                    formatter.dateFormat = "MMM d"
                    lbDescript.text = formatter.string(from: aDate)
                }
            }
        }
    }
}
