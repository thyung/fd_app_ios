/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class MainViewController: BaseViewController {

    @IBOutlet weak var vTable: UITableView!
    @IBOutlet weak var topView: MainTopView!
    //@IBOutlet weak var lbTitleDescript: UILabel!
    
    @IBOutlet weak var topIntervalConstraint: NSLayoutConstraint!
    
    private var eventList: [[String : AnyObject]]?
    private var isFirst = true
    private var isNext: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        requestEvents(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.navigationBar.isHidden = true
        AppUtility.lockOrientation(.portrait)
        UIDevice.current.setValue(UIDeviceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //UIDevice.current.setValue(UIDeviceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    override func setupLayout() {
        let recognizer = UITapGestureRecognizer.init(target: self, action: #selector(taptHandler(_:)))
        topView.addGestureRecognizer(recognizer)
        vTable.backgroundColor = UIColor.white
    }
    
    @objc func taptHandler(_ recognizer: UITapGestureRecognizer?) {
//        self.showIndicator()
//        moveToEvent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HighlightControllerIdentifier"{
            let vc = segue.destination as! HighlightViewController
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            if sender != nil {
                vc.event = sender as? [String : AnyObject]
            }
        } else if segue.identifier == "LiveControllerIdentifier" {
            let vc = segue.destination as! LiveViewController
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            if sender != nil {
                vc.event = sender as? [String : AnyObject]
            }
        }
    }
    
    func requestEvents(_ next: String?) {
        self.showIndicator()
        let message = Event.init()
        //message.event_type = type
        message.next = next
        _ = message.makeRequest()
        LOGD(to: "Request event: \(String(describing: message.cmd)) ",
            "url: \(String(describing: message.request?.description))")
        guard let request = message.request else {
            LOGD(to: "Fail to make event request")
            return
        }
        
        let response = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.contains("json"),
                let aData = data,
                let ret: [String : Any] = aData.dictionaryFromJson(),
                error == nil
                else { return }
            
            //LOGD(to: "\(String(describing: ret))")
            if let events: [[String : AnyObject]] = ret["events"] as? [[String : AnyObject]] {
                self.configEventList(events)
            } else {
                LOGD(to: "Request event not exist field.")
            }
            if let page: [String : AnyObject] = ret["page"] as? [String : AnyObject] {
                if page["next"] != nil && !(page["next"] is NSNull) {
                    self.eventList?.append(page)
                }
            }
            
            DispatchQueue.main.async {
                self.reload()
                self.closeIndicator()
            }
        }
        FDConnection.dataTaskWithRequest(request, block: response)
    }
    
    func configEventList(_ events: [[String : AnyObject]]) {
        LOGD(to: events)
        let array = NSMutableArray(array: events, copyItems: true)
        if isFirst == true {
            if let event = events.first, let status = event["event_status"] as? String {
                if status == "live" || status == "scheduled" {
                    DispatchQueue.main.async { [self] in
                        topView.isHidden = false
                        configTitle(event)
                        topView.configView(event)
                    }
                    array.removeObject(at: 0)
                } else {
                    DispatchQueue.main.async { [self] in
                        topView.isHidden = true
                        topIntervalConstraint.isActive = true
                    }
                }
            } else {
                DispatchQueue.main.async { [self] in
                    topView.isHidden = true
                    topIntervalConstraint.isActive = true
                }
                LOGD(to: "Not exist event.")
            }
        }
        
        if eventList == nil || eventList!.count < 1 {
            self.eventList = array as? [[String : AnyObject]]
        } else {
            let obj: [String : AnyObject] = (eventList?.last)!
            if obj["next"] != nil {
                eventList?.removeLast()
            }
            eventList?.append(contentsOf: (array as? [[String : AnyObject]])!)
        }
    }
        
    private func configTitle(_ event: [String : AnyObject]) {
        var aString = ""
        if let aDate = event["scheduled_at"] as? String {
            let schedule = FDUtill.getDateFromString("EEE, MMM dd,",
                                                     date: aDate,
                                                     timeZone: event["venue_timezone_name"] as? String)
            aString = schedule
        }
        if let venu = event["venue_name"] as? String {
            aString = aString + " | " + venu
        }
        //lbTitleDescript.text = aString
    }
    
    func reload() {
        vTable.reloadData()
    }

    // MARK: - Button Action
    @IBAction func refreshButtonTouchUpInside(_ sender: Any?) {
        isFirst = true
        eventList?.removeAll()
        requestEvents(nil)
    }
    
    @IBAction func watchButtonTouchUpInside(_ sender: AnyObject?) {
        if let obj: [String : AnyObject] = topView.eventInfo as [String : AnyObject]? {
            self.showIndicator()
            prepareEventView(obj)
        }
    }
    
    private func prepareEventView(_ event: [String : AnyObject]) {
        guard let id = event["id"] as? String else { return }
        let message = Event.init()
        message.isStatus = true
        message.id = id
        _ = message.makeRequest()
        LOGD(to: "Request event: \(String(describing: message.cmd)) ",
             "url: \(String(describing: message.request?.description))")
        guard let request = message.request else {
            LOGD(to: "Fail to make event request")
            return
        }
        let response = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.contains("json"),
                let aData = data,
                let ret: [String : Any] = aData.dictionaryFromJson(),
                error == nil
            else { return }
            if let content: [String : AnyObject] = ret["event"] as? [String : AnyObject] {
                if let status = content["event_status"] as? String {
                    DispatchQueue.main.async {
                        switch status {
                        case "scheduled":
                            self.showMessageAlert(message: MSG_GAME_SCHEDULED_MESSAGE, title: MSG_TITLE_NOTI)
                            //self.performSegue(withIdentifier: "LiveControllerIdentifier", sender: event)
                            break
                        case "delayed":
                            self.showMessageAlert(message: MSG_GAME_DELAYED_MESSAGE, title: MSG_GAME_DELAYED_TITLE)
                            break
                        case "canceled":
                            self.showMessageAlert(message: MSG_GAME_CANCELED_MESSAGE, title: MSG_GAME_CANCELED_TITLE)
                            break
                        case "live":
                            self.performSegue(withIdentifier: "LiveControllerIdentifier", sender: event)
                            break
                        case "finished", "highlight":
                            self.performSegue(withIdentifier: "HighlightControllerIdentifier", sender: event)
                            break
                        default:
                            break
                        }
                        self.closeIndicator()
                    }
                } else {
                    DispatchQueue.main.async {                        
                        self.showMessageAlert(message: MSG_GAME_AIRED_YET, title: MSG_TITLE_NOTI)
                        // TODO: Temporoary
                        //self.performSegue(withIdentifier: "LiveControllerIdentifier", sender: event)
                    }
                }
            }
            self.closeIndicator()
        }
        FDConnection.dataTaskWithRequest(request, block: response)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("thyung>>>>>> \(#function) count: \(String(describing: eventList?.count))");
        return eventList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let obj: [String : AnyObject] = eventList?[indexPath.row] {
            if obj["next"] == nil {
                if let cell: EventCell = tableView.dequeueReusableCell(withIdentifier: "ContentIdentifier",
                                                                            for: indexPath) as? EventCell {
                    cell.configCell(obj)
                    return cell
                }
            } else {
                if let cell: NextPageCell = tableView.dequeueReusableCell(withIdentifier: "NextPageIndentifier",
                                                                          for: indexPath) as? NextPageCell {
                    if (tableView.indexPathsForVisibleRows?.contains(indexPath))! {
                        if let next: String = obj["next"] as? String  {
                            cell.startIndicator()
                            requestEvents(next)
                        }
                    } else {
                        cell.stopIndicator()
                    }
                    
                    return cell
                }
            }
        }
        
        return NextPageCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LOGD(to: "\(#function) index: \(indexPath.row)");        
        if let obj: [String : AnyObject] = eventList?[indexPath.row], let count = obj["vod_count"] as? Int {
            if count > 0 {
                prepareEventView(obj)
            } else {
                prepareEventView(obj)
                //self.showMessageAlert(message: MSG_GAME_SCHEDULED_MESSAGE, title: MSG_TITLE_NOTI)
            }
        }
    }
}
