/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var ivLeft: UIImageView!
    @IBOutlet weak var lbLeftName: UILabel!
    @IBOutlet weak var lbLeftScore: UILabel!
    @IBOutlet weak var lbRightScore: UILabel!
    @IBOutlet weak var ivRight: UIImageView!
    @IBOutlet weak var lbRightName: UILabel!
    @IBOutlet weak var ivStatus: UIImageView!
    @IBOutlet weak var lbDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ event: Dictionary<String, Any>) {
        lbDescription.text = event["description"] as? String
        if let info: Dictionary<String, Any> = event["event_meta_info"] as? Dictionary<String, Any> {
            updateThumnail(info)
            lbLeftName.text = info["home_team_name"] as? String
            lbRightName.text = info["away_team_name"] as? String
            lbLeftScore.text = "\(info["home_team_score"] as? Int ?? 0)"
            lbRightScore.text = "\(info["away_team_score"] as? Int ?? 0)"
            
            //lbDescription.text = (info["match_type"] as? String ?? "") + " | " + (info["weight_class"] as? String ?? "")
        }
        if let status = event["event_status"] as? String {
            var image: String? = nil
            switch status {
            case "live":
                image = "tag_list_live_normal"
                break
            case "finished":
                image = "tag_list_finished_normal"
                break
            case "highlight":
                image = "tag_list_4dhighlight_normal"
                break;
            case "postpone":
                image = "tag_list_postpone"
                lbLeftScore.isHidden = true
                lbRightScore.isHidden = true
                break;
            case "cancel":
                image = "tag_list_cancel"
                lbLeftScore.isHidden = true
                lbRightScore.isHidden = true
                break;
            default:
                break
            }
            if image != nil {
                ivStatus.image = UIImage(named: image!)
                ivStatus.isHidden = false
            } else {
                ivStatus.isHidden = true
            }
        }
        
        let formatter = DateFormatter()
        if let timeZone = event["venue_timezone_name"] as? String {
            formatter.timeZone = TimeZone(identifier: timeZone)
            let temp =  formatter.timeZone.abbreviation()
            if let aString = event["scheduled_at"] as? String {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                let aDate: Date = formatter.date(from: aString)!
                formatter.dateFormat = "MMM d, HH:mm"
                lbDescription.text = formatter.string(from: aDate) + " (" + (temp ?? "") + ")"
            }
        } else {
            if let aString = event["scheduled_at"] as? String {
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                let aDate: Date = formatter.date(from: aString)!
                formatter.dateFormat = "MMM d, HH:mm"
                lbDescription.text = formatter.string(from: aDate)
            }
        }
    }
    
    func updateThumnail(_ info: Dictionary<String, Any>) {
        let leftUrl = info["home_team_logo_url"] as? String ?? ""
        let rightUrl = info["away_team_logo_url"] as? String ?? ""
        FDConnection.requestImageWithTarget(ivLeft, url: leftUrl)
        FDConnection.requestImageWithTarget(ivRight, url: rightUrl)
    }
}
