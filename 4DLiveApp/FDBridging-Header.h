/*
 * Copyright (c) 2021 4dreplay Co., Ltd.
 * All rights reserved.
 */

#ifndef FDBridging_Header_h
#define FDBridging_Header_h

#import <FDLive/FDLiveSettings.h>
#import <FDLive/FDLiveDefines.h>
#import <FDLive/FDLivePlayer.h>
#import "Reachability.h"

#define FD_VERSION_STRING @"1.0.0.2020XXXX00"
#define FD_VERSION_NUMBER @"10000"

#define DATA_MODEL          (@"Model")

#endif /* FDBridging_Header_h */
